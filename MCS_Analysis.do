capture log close
version 15.1
clear all
macro drop _all
set linesize 100

* Set current directory
	cd "\\storage1.ris.wustl.edu\kjoyntmaddox\Active\Daniel\MCS Disparities Manoj"

////////////////////////////////////////////////////////////////////
* Table 1: Patient Characteristics

	* Create log file
		log using "Output\T1 Patients", replace text
	
	* Overall
		use "New Data\cardshock1217.dta"
		local tot_cols = 2
		local tot_rows = 2

		putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
		local Cell = char(64 + 2) + string(`tot_rows')
		putexcel `Cell' = "All", hcenter
		local Cell = char(64 + 1) + string(`tot_rows' + 1)
		putexcel `Cell' = "Total (N)"
		local Cell = char(64 + `tot_cols') + string(`tot_rows' + 1)
		putexcel `Cell' = _N
		
		local tot_rows = `tot_rows' + 1
		
		foreach RowVar in agecat FEMALE race2 insurance DX1_MI_tot pci_tot iabp_tot tand_imp_tot ecmo_tot TOTCHG lowinc CM_LYTES CM_HTN_C CM_RENLFAIL CM_CHRNLUNG CM_DM CM_CHF CM_ANEMDEF CM_COAG CM_OBESE CM_PERIVASC CM_DMCX CM_NEURO CM_DEPRESS CM_VALVE CM_PULMCIRC CM_ALCOHOL CM_LIVER CM_DRUG {
					
			if `RowVar' == TOTCHG {
				local binary 0
				quietly sum `RowVar'
				local MeanChargesAll = round(r(mean), 0.01)
				quietly sum `RowVar' if mechsupp == 1
				local MeanChargesMCS = round(r(mean), 0.01)
				matrix rowtotals = (`MeanChargesAll' \ `MeanChargesMCS')
				local RowCount = 2
			}
			else {
				quietly tab `RowVar', matcell(rowtotals)
				local binary = r(r) - 1
				local TotalCount = r(N)

				if `binary' == 1 { 
					tab `RowVar' if `RowVar' == 1, matcell(cellcounts)
				}
				else {
					tab `RowVar', matcell(cellcounts)
				}
				
				local RowCount = r(r)
				local ColCount = r(c)
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
			}
			 
			putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
			if `tot_cols' == 2 {
				local Cell = char(64 + 1) + string(1 + `tot_rows')
				putexcel `Cell' = "`RowVar'", left
			}
			
			forvalues row = 1/`RowCount' {		
				
				if `binary' == 1 {
					local cellcount = rowtotals[2,1]
					local Cell = char(64 + `tot_cols') + string(`row' + `tot_rows')
				}
				else {
					local RowValueLabelNum = word("`RowLevels'", `row')
					local Cell = char(64 + 1) + string(`row' + 1 + `tot_rows')
					if `RowVar' == TOTCHG {
						if `row' == 1 {
							local CellContents = "Cardiogenic Shock Admissions"
						}
						if `row' == 2 {
							local CellContents = "Only Admissions with MCS"
						}
					}
					else {
						local CellContents : label `RowValueLabel' `RowValueLabelNum'
					}
					putexcel `Cell' = "`CellContents'", right
					local cellcount = rowtotals[`row',1]
					
					local Cell = char(64 + `tot_cols') + string(`row' + 1 +`tot_rows')
				}
				
				if `RowVar' == TOTCHG {
					local CellContents = "$`cellcount'"
				}
				else {
					local cellpercent = string(100*`cellcount'/`TotalCount',"%9.1f")
					local CellContents = "`cellcount' (`cellpercent'%)"
				}
				putexcel `Cell' = "`CellContents'"
			}
			
			if `binary' == 1 {
				local tot_rows = `tot_rows' + `RowCount'	
			}
			else {
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			
			if `tot_rows' == 30 {
				local tot_rows = `tot_rows' + 1 //For "Clinical Comorbidities" label
			}		
		}

	* Table 1 Groups 1 and 2
		local tot_cols = 3
		local tot_rows = 3

		putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
		tab ischemia, matcell(cellcounts)
		local ColCount = r(r)
		local TotalCount = r(N)
		local Cell = char(64 + `tot_cols') + string(`tot_rows')
		putexcel `Cell' = `TotalCount'
		forvalues col = 1/`ColCount' {
			local cellcount = cellcounts[`col',1]
			local cellpercent = string(100*`cellcount'/`TotalCount',"%9.1f")
			local CellContents = "`cellcount' (`cellpercent'%)"
			local Cell = char(64 + `tot_cols' + 2 - `col') + string(3)
			putexcel `Cell' = "`CellContents'", right
		}

		foreach RowVar in agecat FEMALE race2 insurance DX1_MI_tot pci_tot iabp_tot tand_imp_tot ecmo_tot TOTCHG lowinc CM_LYTES CM_HTN_C CM_RENLFAIL CM_CHRNLUNG CM_DM CM_CHF CM_ANEMDEF CM_COAG CM_OBESE CM_PERIVASC CM_DMCX CM_NEURO CM_DEPRESS CM_VALVE CM_PULMCIRC CM_ALCOHOL CM_LIVER CM_DRUG {
			local ColVar = "ischemia"
				
			if `RowVar' == TOTCHG {
				local binary 0
				su TOTCHG if ischemia == 0
				local MeanChargesAll2 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 1
				local MeanChargesAll1 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 0 & mechsupp == 1
				local MeanChargesMCS2 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 1 & mechsupp == 1	
				local MeanChargesMCS1 = round(r(mean), 0.01)
				matrix cellcounts = (`MeanChargesAll2', `MeanChargesAll1' \ `MeanChargesMCS2', `MeanChargesMCS1')
				local RowCount = 2
				local ColCount = 2
			}
			else {
				quietly tab `RowVar' if !missing(`ColVar'), matcell(rowtotals)
				local binary = r(r) - 1
				quietly tab `ColVar' if !missing(`RowVar'), matcell(coltotals)
				quietly tab `RowVar' `ColVar', chi2
				local PVal = r(p)
				local TotalCount = r(N)
				
				if `binary' == 1 { 
					if `RowVar' == DX1_MI_tot | `RowVar' == pci_tot {
						tab `RowVar' `ColVar', matcell(fullcellcounts) chi2
						matrix cellcounts = (fullcellcounts[2,1], fullcellcounts[2,2])
						local RowCount = 1
						local ColCount = 2
					}
					else {
						tab `RowVar' `ColVar' if `RowVar' == 1, matcell(cellcounts) chi2
						local RowCount = r(r)
						local ColCount = r(c)
					}
				}
				else {
					tab `RowVar' `ColVar', matcell(cellcounts) chi2
					local RowCount = r(r)
					local ColCount = r(c)
				}
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
				 
				local ColValueLabel : value label `ColVar'
				levelsof `ColVar', local(ColLevels)
			}
			
			putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
			forvalues row = 1/`RowCount' {
				forvalues col = `ColCount'(-1)1 {
					local cellcount = cellcounts[`row',`col']
					
					if `RowVar' == TOTCHG {
						local CellContents = "$`cellcount'"
					}
					else {
						local cellpercent = string(100*`cellcount'/coltotals[`col',1],"%9.1f")
						local CellContents = "`cellcount' (`cellpercent'%)"
					}
					
					if `binary' == 1 {
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(`row' + `tot_rows')
					}
					else {
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(`row' + 1 + `tot_rows')
					}
					
					putexcel `Cell' = "`CellContents'", right
					
					if `row'==1 {
						local ColValueLabelNum = word("`ColLevels'", `col')
						local CellContents : label `ColValueLabel' `ColValueLabelNum'
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(2)
						putexcel `Cell' = "`CellContents'", hcenter
					}
				}
			}
			
			if `binary' == 1 {
				local tot_rows = `tot_rows' + `RowCount'
			}
			else {
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			
			if `tot_rows' == 30 {
				local tot_rows = `tot_rows' + 1 //For "Clinical Comorbidities" label
			}
		}

	* Table 1 MCS Recipients
		local tot_cols = 5
		local tot_rows = 3

		putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
		tab mechsupp, matcell(cellcounts)
		local ColCount = r(r)
		local TotalCount = r(N)
		local Cell = char(64 + `tot_cols') + string(`tot_rows')
		putexcel `Cell' = `TotalCount'
		forvalues col = 1/`ColCount' {
			local cellcount = cellcounts[`col',1]
			local cellpercent = string(100*`cellcount'/`TotalCount',"%9.1f")
			local CellContents = "`cellcount' (`cellpercent'%)"
			local Cell = char(64 + `tot_cols' + 2 - `col') + string(3)
			putexcel `Cell' = "`CellContents'", right
		}

		foreach RowVar in agecat FEMALE race2 insurance DX1_MI_tot pci_tot iabp_tot tand_imp_tot ecmo_tot TOTCHG lowinc CM_LYTES CM_HTN_C CM_RENLFAIL CM_CHRNLUNG CM_DM CM_CHF CM_ANEMDEF CM_COAG CM_OBESE CM_PERIVASC CM_DMCX CM_NEURO CM_DEPRESS CM_VALVE CM_PULMCIRC CM_ALCOHOL CM_LIVER CM_DRUG {
			local ColVar = "mechsupp"
				
			if `RowVar' == TOTCHG {
				local binary 0
				su TOTCHG if mechsupp == 0
				local MeanChargesAll2 = round(r(mean), 0.01)
				su TOTCHG if mechsupp == 1
				local MeanChargesAll1 = round(r(mean), 0.01)
				su TOTCHG if mechsupp == 0 & mechsupp == 1
				local MeanChargesMCS2 = round(r(mean), 0.01)
				su TOTCHG if mechsupp == 1 & mechsupp == 1	
				local MeanChargesMCS1 = round(r(mean), 0.01)
				matrix cellcounts = (`MeanChargesAll2', `MeanChargesAll1' \ `MeanChargesMCS2', `MeanChargesMCS1')
				local RowCount = 2
				local ColCount = 2
			}
			else {
				quietly tab `RowVar' if !missing(`ColVar'), matcell(rowtotals)
				local binary = r(r) - 1
				quietly tab `ColVar' if !missing(`RowVar'), matcell(coltotals)
				quietly tab `RowVar' `ColVar', chi2
				local PVal = r(p)
				local TotalCount = r(N)
				
				if `binary' == 1 { 
					if `RowVar' == DX1_MI_tot | `RowVar' == pci_tot {
						tab `RowVar' `ColVar', matcell(fullcellcounts) chi2
						matrix cellcounts = (fullcellcounts[2,1], fullcellcounts[2,2])
						local RowCount = 1
						local ColCount = 2
					}
					else {
						tab `RowVar' `ColVar' if `RowVar' == 1, matcell(cellcounts) chi2
						local RowCount = r(r)
						local ColCount = r(c)
					}
				}
				else {
					tab `RowVar' `ColVar', matcell(cellcounts) chi2
					local RowCount = r(r)
					local ColCount = r(c)
				}
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
				 
				local ColValueLabel : value label `ColVar'
				levelsof `ColVar', local(ColLevels)
			}
			
			putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify
			forvalues row = 1/`RowCount' {
				forvalues col = `ColCount'(-1)1 {
					local cellcount = cellcounts[`row',`col']
					
					if `RowVar' == TOTCHG {
						local CellContents = "$`cellcount'"
					}
					else {
						local cellpercent = string(100*`cellcount'/coltotals[`col',1],"%9.1f")
						local CellContents = "`cellcount' (`cellpercent'%)"
					}
					
					if `binary' == 1 {
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(`row' + `tot_rows')
					}
					else {
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(`row' + 1 + `tot_rows')
					}
					
					putexcel `Cell' = "`CellContents'", right
					
					if `row'==1 {
						local ColValueLabelNum = word("`ColLevels'", `col')
						local CellContents : label `ColValueLabel' `ColValueLabelNum'
						local Cell = char(64 + `tot_cols' - `col' + 2) + string(2)
						putexcel `Cell' = "`CellContents'", hcenter
					}
				}
			}
			
			if `binary' == 1 {
				local tot_rows = `tot_rows' + `RowCount'
			}
			else {
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			
			if `tot_rows' == 30 {
				local tot_rows = `tot_rows' + 1 //For "Clinical Comorbidities" label
			}
		}
		
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify

		* Column 1
			putexcel A1 = "Table I: Patient Characteristics Among Admissions for Cardiogenic Shock.", left 
			putexcel A2 = "Characteristic", left
			putexcel E2 = "MCS Recipients", left
			putexcel F2 = "No MCS Recipients", left
			putexcel A4 = "Age", left
			putexcel A10 = "Female", left
			putexcel A11 = "Race", left
			putexcel A15 = "Other/Unknown", right
			putexcel A16 = "Insurance", left
			putexcel A17 = "Medicare", right
			putexcel A18 = "Medicaid", right
			putexcel A19 = "Private Insurance", right
			putexcel A20 = "Uninsured", right
			putexcel A21 = "Other", right
			putexcel A22 = "Primary admitting diagnosis of myocardial infarction", left
			putexcel A23 = "PCI during hospitalization", left
			putexcel A24 = "IABP during hospitalization", left
			putexcel A25 = "PVAD during hospitalization", left
			putexcel A26 = "ECMO during hospitalization", left
			putexcel A27 = "Average Charges", left
			putexcel A30 = "Lowest Income Quartile", left
			putexcel A31 = "Selected Comorbidities", left
			putexcel A32 = "Fluid and Electrolyte Disorders", right
			putexcel A33 = "Hypertension", right
			putexcel A34 = "Renal Failure", right
			putexcel A35 = "Chronic Lung Disease", right
			putexcel A36 = "Diabetes", right
			putexcel A37 = "Congestive Heart Failure", right
			putexcel A38 = "Anemia", right
			putexcel A39 = "Coagulopathy", right
			putexcel A40 = "Obesity", right
			putexcel A41 = "Peripheral Vascular Disease", right
			putexcel A42 = "Diabetes with Complications", right
			putexcel A43 = "Neurologic Disorders", right
			putexcel A44 = "Depression", right
			putexcel A45 = "Valvular Disease", right
			putexcel A46 = "Pulmonary Circulation Disorders", right
			putexcel A47 = "Alcohol Use", right
			putexcel A48 = "Liver Failure", right
			putexcel A49 = "Drug Use", right

	* Formatting
		putexcel set "Output\excelresults.xlsx", sheet(T1 Patients) modify

		putexcel A1:A49, bold
		putexcel A2:F2, bold

		putexcel A3:F3, fpattern(solid, "204 204 204")
		putexcel A5:F5, fpattern(solid, "204 204 204")
		putexcel A7:F7, fpattern(solid, "204 204 204")
		putexcel A9:F9, fpattern(solid, "204 204 204")
		putexcel A11:F11, fpattern(solid, "204 204 204")
		putexcel A13:F13, fpattern(solid, "204 204 204")
		putexcel A15:F15, fpattern(solid, "204 204 204")
		putexcel A17:F17, fpattern(solid, "204 204 204")
		putexcel A19:F19, fpattern(solid, "204 204 204")
		putexcel A21:F21, fpattern(solid, "204 204 204")
		putexcel A23:F23, fpattern(solid, "204 204 204")
		putexcel A25:F25, fpattern(solid, "204 204 204")
		putexcel A27:F27, fpattern(solid, "204 204 204")
		putexcel A29:F29, fpattern(solid, "204 204 204")
		putexcel A31:F31, fpattern(solid, "204 204 204")
		putexcel A33:F33, fpattern(solid, "204 204 204")
		putexcel A35:F35, fpattern(solid, "204 204 204")
		putexcel A37:F37, fpattern(solid, "204 204 204")
		putexcel A39:F39, fpattern(solid, "204 204 204")
		putexcel A41:F41, fpattern(solid, "204 204 204")
		putexcel A43:F43, fpattern(solid, "204 204 204")
		putexcel A45:F45, fpattern(solid, "204 204 204")
		putexcel A47:F47, fpattern(solid, "204 204 204")
		putexcel A49:F49, fpattern(solid, "204 204 204")

		putexcel B2:G49, border(left, medium, black)
		putexcel A1:F49, border(bottom, medium, black)
		putexcel A2:F2, border(bottom, thick, black)

	* Close log file
		log close	
		
////////////////////////////////////////////////////////////////////
* Table 2 Hospital Characteristics

	* Create log file
		log using "Output\T2 Hospitals", replace text

	* Keep hospitals in 2017
		keep if YEAR==2017
		
	* Delete duplicate hospitals
		sort HOSP_NIS
		quietly by HOSP_NIS: gen dup = cond(_N==1,0,_n)
		drop if dup>1

	* Table 2
		putexcel set "Output\excelresults.xlsx", sheet(T2 Hospitals) modify

		local tot_rows = 1
		foreach RowVar in HOSP_BEDSIZE H_CONTRL HOSP_LOCTEACH HOSP_REGION {
			
			quietly tab `RowVar', matcell(rowtotals)
			local RowCount = r(r)
			local TotalCount = r(N)
			 
			local RowValueLabel : value label `RowVar'
			levelsof `RowVar', local(RowLevels)
			
			local Cell = char(64 + 1) + string(3 + `tot_rows')
			putexcel `Cell' = "	`RowVar'", left bold
			
			forvalues row = 1/`RowCount' {
			 
				local RowValueLabelNum = word("`RowLevels'", `row')
				local CellContents : label `RowValueLabel' `RowValueLabelNum'	
				local Cell = char(64 + 1) + string(`row' + 3 + `tot_rows')
				putexcel `Cell' = "`CellContents'", right
				
				local cellcount = rowtotals[`row',1]
				local CellContents = "`cellcount'"
				local Cell = char(64 + 2) + string(`row' + 3 + `tot_rows')
				putexcel `Cell' = "`CellContents'", hcenter
				
				local cellpercent = string(100*`cellcount'/`TotalCount',"%9.1f")
				local CellContents = "`cellpercent'"
				local Cell = char(64 + 3) + string(`row' + 3 + `tot_rows')
				putexcel `Cell' = "`CellContents'", hcenter
			}	
			local tot_rows = `tot_rows' + `RowCount' + 1
		}

	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(T2 Hospitals) modify
		putexcel A1 = "Table II: Hospital Characteristics.", left bold
		local total = _N
		putexcel A2 = "Total = `total'", left bold
		
		* Row 1
			putexcel A3 = "Characteristic", left bold
			putexcel C3 = "n", left bold
			putexcel C3 = "%", left bold
		
		* Column 1
			putexcel A4 = "Hospital Size", left bold
			putexcel A8 = "Ownership", left bold
			putexcel A9 = "Government"
			putexcel A10 = "Non-Profit Private"
			putexcel A11 = "Invester Owned Private"
			putexcel A12 = "Teaching Status", left bold
			putexcel A14 = "Urban Non-teaching"
			putexcel A15 = "Urban Teaching"
			putexcel A16 = "Region", left bold
			putexcel A17 = "Northeast"
			putexcel A18 = "Midwest"
			putexcel A19 = "South"
			putexcel A20 = "West"

	* Formatting
		putexcel set "Output\excelresults.xlsx", sheet(T2 Hospitals) modify

		putexcel A4:C4, fpattern(solid, "217 217 217")
		putexcel A6:C6, fpattern(solid, "217 217 217")
		putexcel A8:C8, fpattern(solid, "217 217 217")
		putexcel A10:C10, fpattern(solid, "217 217 217")
		putexcel A12:C12, fpattern(solid, "217 217 217")
		putexcel A14:C14, fpattern(solid, "217 217 217")
		putexcel A16:C16, fpattern(solid, "217 217 217")
		putexcel A18:C18, fpattern(solid, "217 217 217")
		putexcel A20:C20, fpattern(solid, "217 217 217")
		putexcel A3:A20, border(left, medium, black)
		putexcel C3:C20, border(right, medium, black)
		putexcel A2:C3, border(bottom, medium, black)
		putexcel A20:C20, border(bottom, medium, black)

	* Close log file
		log close	
		
	* Clear data
		clear

////////////////////////////////////////////////////////////////////
* Table 3 Rate of Use of Mechanical Circulatory Support

	* Create log file
		log using "Output\T3 MCS", replace text
		
	* Rates of MCS
		use "New Data\cardshock1217.dta"
		local tot_cols = 2
		local tot_rows = 3
		
		forvalues x = 1/3 {
			putexcel set "Output\excelresults.xlsx", sheet(T3 MCS) modify
			local Cell = char(64 + `tot_cols') + string(`tot_rows')
			if `x' == 1 {
				putexcel `Cell' = "All Rate", hcenter
			}
			if `x' == 2 {
				putexcel `Cell' = "Group 1 Rate", hcenter
			}
			if `x' == 3 {
				putexcel `Cell' = "Group 2 Rate", hcenter
			}
			
			foreach RowVar in FEMALE race2 insurance lowinc {
				if `x' == 1 {
					quietly tab `RowVar', matcell(rowtotals)
					tab `RowVar' mechsupp if mechsupp == 1, matcell(cellcounts)
				}
				if `x' == 2 {
					quietly tab `RowVar' if ischemia == 1, matcell(rowtotals)
					tab `RowVar' mechsupp if mechsupp == 1 & ischemia == 1, matcell(cellcounts)
				}
				if `x' == 3 {
					quietly tab `RowVar' if ischemia == 0, matcell(rowtotals)
					tab `RowVar' mechsupp if mechsupp == 1 & ischemia == 0, matcell(cellcounts)
				}
				
				local RowCount = r(r)
				local ColCount = r(c)
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
				
				if `tot_cols' == 2 {
					local Cell = char(64 + 1) + string(1 + `tot_rows')
					putexcel `Cell' = "`RowVar'", left
				}
				
				forvalues row = 1/`RowCount' {		
					if `tot_cols' == 2 {
						local RowValueLabelNum = word("`RowLevels'", `row')
						local Cell = char(64 + 1) + string(`row' + 1 + `tot_rows')
						local CellContents : label `RowValueLabel' `RowValueLabelNum'
						putexcel `Cell' = "`CellContents'", right
					}
					local cellcount = cellcounts[`row',1]
					
					local Cell = char(64 + `tot_cols') + string(`row' + 1 +`tot_rows')
					local cellpercent = string(100*`cellcount'/rowtotals[`row',1],"%9.1f")
					local CellContents = "`cellpercent'%"
					putexcel `Cell' = "`CellContents'"
				}
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			local tot_cols = `tot_cols' + 3
			local tot_rows = 3
		}

	* Fully-adjusted odds
		local tot_cols = 3
		local tot_rows = 5
		
		forvalues x = 1/3 {
			putexcel set "Output\excelresults.xlsx", sheet(T3 MCS) modify
			local Cell = char(64 + `tot_cols') + string(`tot_rows'-2)
			if `x' == 1 {
				putexcel `Cell' = "All Odds", hcenter
				glm mechsupp i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
			
			if `x' == 2 {
				putexcel `Cell' = "Group 1 Odds", hcenter
				glm mechsupp i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION if ischemia == 1, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
			
			if `x' == 3 {
				putexcel `Cell' = "Group 2 Odds", hcenter
				glm mechsupp i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION if ischemia == 0, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
					
			matrix full_results = r(table)'
			matrix results = full_results[1..13,1], full_results[1..13,4]
			
			forvalues row = 1/13 {
				forvalues col = 1/2 {
					if `col' == 1 {
						local CellContents = string(results[`row',`col'], "%9.2f")
					}
					else {
						local CellContents = string(results[`row',`col'], "%9.3f")
					}
					local Cell = char(64 + `tot_cols' + `col' - 1) + string(`tot_rows')
					putexcel `Cell' = `CellContents'
				}
				local tot_rows = `tot_rows' + 1
				
				if `tot_rows' == 7 | `tot_rows' == 12 | `tot_rows' == 18 {
					local tot_rows = `tot_rows' + 1 //For labels
				}
			}
			local tot_cols = `tot_cols' + 3
			local tot_rows = 5
		}	
		
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(T3 MCS) modify

		* Column 1
			putexcel A1 = "Table III: Rate of Use of Mechanical Circulatory Support.", left bold
			putexcel A3 = "Characteristic", left
			putexcel A4 = "Sex", left
			putexcel A7 = "Race", left
			putexcel A11 = "Other", right
			putexcel A12 = "Insurance", left
			putexcel A18 = "Zip Income", left
			
		* Row 1
			putexcel B2:D2 = "All", hcenter merge
			putexcel E2:G2 = "Group 1", hcenter merge
			putexcel H2:J2 = "Group 2", hcenter merge
			
		* Row 2
			putexcel B3 = "Rate of MCS", hcenter
			putexcel C3 = "Adjusted Odds", hcenter
			putexcel D3 = "p-value", hcenter
			putexcel E3 = "Rate of MCS", hcenter
			putexcel F3 = "Adjusted Odds", hcenter
			putexcel G3 = "p-value", hcenter
			putexcel H3 = "Rate of MCS", hcenter
			putexcel I3 = "Adjusted Odds", hcenter
			putexcel J3 = "p-value", hcenter
			
		* Ref
			putexcel C5:D5 = "Ref"
			putexcel F5:G5 = "Ref"
			putexcel I5:J5 = "Ref"
			putexcel C8:D8 = "Ref"
			putexcel F8:G8 = "Ref"
			putexcel I8:J8 = "Ref"
			putexcel C15:D15 = "Ref"
			putexcel F15:G15 = "Ref"
			putexcel I15:J15 = "Ref"
			putexcel C19:D19 = "Ref"
			putexcel F19:G19 = "Ref"
			putexcel I19:J19 = "Ref"

	* Formatting
		putexcel set "Output\excelresults.xlsx", sheet(T3 MCS) modify

		putexcel A1:A20, bold
		putexcel B2:J3, bold hcenter
		putexcel B4:J20, hcenter

		putexcel A4:J4, fpattern(solid, "217 217 217")
		putexcel A6:J6, fpattern(solid, "217 217 217")
		putexcel A8:J8, fpattern(solid, "217 217 217")
		putexcel A10:J10, fpattern(solid, "217 217 217")
		putexcel A12:J12, fpattern(solid, "217 217 217")
		putexcel A14:J14, fpattern(solid, "217 217 217")
		putexcel A16:J16, fpattern(solid, "217 217 217")
		putexcel A18:J18, fpattern(solid, "217 217 217")
		putexcel A20:J20, fpattern(solid, "217 217 217")

		putexcel B2:K20, border(left, medium, black)
		putexcel A1:J20, border(bottom, medium, black)
		putexcel A3:J3, border(bottom, thick, black)

	* Close log file
		log close
		
///////////////////////////////////////////////////////////////////	

* Table 4 Mortality Among All Cardiogenic Shock Admissions 

	* Create log file
		log using "Output\T4 Mortality", replace text
		
	* Mortality
		local tot_cols = 2
		local tot_rows = 3
		
		forvalues x = 1/3 {
			putexcel set "Output\excelresults.xlsx", sheet(T4 Mortality) modify
			local Cell = char(64 + `tot_cols') + string(`tot_rows')
			if `x' == 1 {
				putexcel `Cell' = "All Mortality", hcenter
			}
			if `x' == 2 {
				putexcel `Cell' = "Group 1 Mortality", hcenter
			}
			if `x' == 3 {
				putexcel `Cell' = "Group 2 Mortality", hcenter
			}
			
			foreach RowVar in FEMALE race2 insurance lowinc {
				if `x' == 1 {
					quietly tab `RowVar', matcell(rowtotals)
					tab `RowVar' DIED if DIED == 1, matcell(cellcounts)
				}
				if `x' == 2 {
					quietly tab `RowVar' if ischemia == 1, matcell(rowtotals)
					tab `RowVar' DIED if DIED == 1 & ischemia == 1, matcell(cellcounts)
				}
				if `x' == 3 {
					quietly tab `RowVar' if ischemia == 0, matcell(rowtotals)
					tab `RowVar' DIED if DIED == 1 & ischemia == 0, matcell(cellcounts)
				}
				
				local RowCount = r(r)
				local ColCount = r(c)
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
				
				if `tot_cols' == 2 {
					local Cell = char(64 + 1) + string(1 + `tot_rows')
					putexcel `Cell' = "`RowVar'", left
				}
				
				forvalues row = 1/`RowCount' {		
					if `tot_cols' == 2 {
						local RowValueLabelNum = word("`RowLevels'", `row')
						local Cell = char(64 + 1) + string(`row' + 1 + `tot_rows')
						local CellContents : label `RowValueLabel' `RowValueLabelNum'
						putexcel `Cell' = "`CellContents'", right
					}
					local cellcount = cellcounts[`row',1]
					
					local Cell = char(64 + `tot_cols') + string(`row' + 1 +`tot_rows')
					local cellpercent = string(100*`cellcount'/rowtotals[`row',1],"%9.1f")
					local CellContents = "`cellpercent'%"
					putexcel `Cell' = "`CellContents'"
				}
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			local tot_cols = `tot_cols' + 3
			local tot_rows = 3
		}
	
	* Fully-adjusted odds
		local tot_cols = 3
		local tot_rows = 5
		
		forvalues x = 1/3 {
			putexcel set "Output\excelresults.xlsx", sheet(T4 Mortality) modify
			local Cell = char(64 + `tot_cols') + string(`tot_rows'-2)
			if `x' == 1 {
				putexcel `Cell' = "All Odds", hcenter
				glm DIED i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION mechsupp, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
			
			if `x' == 2 {
				putexcel `Cell' = "Group 1 Odds", hcenter
				glm DIED i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION mechsupp if ischemia == 1, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
			
			if `x' == 3 {
				putexcel `Cell' = "Group 2 Odds", hcenter
				glm DIED i.FEMALE i.race2 ib3.insurance i.lowinc ib5.agecat CM_AIDS-CM_WGHTLOSS ib3.HOSP_BEDSIZE ib2.H_CONTRL ib3.HOSP_LOCTEACH ib3.HOSP_REGION mechsupp if ischemia == 0, eform baselevels link(logit) family(binomial) robust cluster(HOSP_ID_YEAR)
			}
					
			matrix full_results = r(table)'
			matrix results = full_results[1..13,1], full_results[1..13,4]
			
			forvalues row = 1/13 {
				forvalues col = 1/2 {
					if `col' == 1 {
						local CellContents = string(results[`row',`col'], "%9.2f")
					}
					else {
						local CellContents = string(results[`row',`col'], "%9.3f")
					}
					local Cell = char(64 + `tot_cols' + `col' - 1) + string(`tot_rows')
					putexcel `Cell' = `CellContents'
				}
				local tot_rows = `tot_rows' + 1
				
				if `tot_rows' == 7 | `tot_rows' == 12 | `tot_rows' == 18 {
					local tot_rows = `tot_rows' + 1 //For labels
				}
			}
			local tot_cols = `tot_cols' + 3
			local tot_rows = 5
		}	
		
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(T4 Mortality) modify

		* Column 1
			putexcel A1 = "Table IV: Mortality Among All Cardiogenic Shock Admissions.", left bold
			putexcel A3 = "Characteristic", left
			putexcel A4 = "Sex", left
			putexcel A7 = "Race", left
			putexcel A11 = "Other", right
			putexcel A12 = "Insurance", left
			putexcel A18 = "Zip Income", left
			
		* Row 1
			putexcel B2:D2 = "All", hcenter merge
			putexcel E2:G2 = "Group 1", hcenter merge
			putexcel H2:J2 = "Group 2", hcenter merge
			
		* Row 2
			putexcel B3 = "Mortality", hcenter
			putexcel C3 = "Adjusted Odds", hcenter
			putexcel D3 = "p-value", hcenter
			putexcel E3 = "Mortality", hcenter
			putexcel F3 = "Adjusted Odds", hcenter
			putexcel G3 = "p-value", hcenter
			putexcel H3 = "Mortality", hcenter
			putexcel I3 = "Adjusted Odds", hcenter
			putexcel J3 = "p-value", hcenter
			
		* Ref
			putexcel C5:D5 = "Ref"
			putexcel F5:G5 = "Ref"
			putexcel I5:J5 = "Ref"
			putexcel C8:D8 = "Ref"
			putexcel F8:G8 = "Ref"
			putexcel I8:J8 = "Ref"
			putexcel C15:D15 = "Ref"
			putexcel F15:G15 = "Ref"
			putexcel I15:J15 = "Ref"
			putexcel C19:D19 = "Ref"
			putexcel F19:G19 = "Ref"
			putexcel I19:J19 = "Ref"

	* Formatting
		putexcel set "Output\excelresults.xlsx", sheet(T4 Mortality) modify

		putexcel A1:A20, bold
		putexcel B2:J3, bold hcenter
		putexcel B4:J20, hcenter

		putexcel A4:J4, fpattern(solid, "217 217 217")
		putexcel A6:J6, fpattern(solid, "217 217 217")
		putexcel A8:J8, fpattern(solid, "217 217 217")
		putexcel A10:J10, fpattern(solid, "217 217 217")
		putexcel A12:J12, fpattern(solid, "217 217 217")
		putexcel A14:J14, fpattern(solid, "217 217 217")
		putexcel A16:J16, fpattern(solid, "217 217 217")
		putexcel A18:J18, fpattern(solid, "217 217 217")
		putexcel A20:J20, fpattern(solid, "217 217 217")

		putexcel B2:K20, border(left, medium, black)
		putexcel A1:J20, border(bottom, medium, black)
		putexcel A3:J3, border(bottom, thick, black)
		
	* Close log file
		log close

///////////////////////////////////////////////////////////////////	

* Figure 1 Rate of Use of Mechanical Circulatory Support

	* Create log file
		log using "Output\F1B MCS Use", replace text
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(F1 MCS Use) modify
		putexcel A1 = "Figure 1: Rate of Use of Mechanical Circulatory Support", bold left	
		
	* Any MCS
		decode mechsupp, generate(mechsupp_s)
	
		* Female
			tab FEMALE mechsupp, row matcell(cellcounts)

			levelsof mechsupp_s, local(mechsupplabels)
			matrix colnames cellcounts = `mechsupplabels'
			
			matrix list cellcounts
			
			putexcel A2 = matrix(cellcounts), names hcenter
			putexcel A2 = "Any MCS", hcenter
			putexcel A3 = "Male", hcenter
			putexcel A4 = "Female", hcenter
			putexcel D2 = "Total", hcenter
			
		* Race
			tab race2 mechsupp, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B5 = matrix(cellcounts), hcenter
			putexcel A5 = "White", hcenter
			putexcel A6 = "Black", hcenter
			putexcel A7 = "Hispanic", hcenter
			putexcel A8 = "Other Race", hcenter
		
		* Insurance
			tab insurance mechsupp, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B9 = matrix(cellcounts), hcenter
			putexcel A9 = "Medicare", hcenter
			putexcel A10 = "Medicaid", hcenter
			putexcel A11 = "Private", hcenter
			putexcel A12 = "Uninsured", hcenter
		
		* Income
			tab lowinc mechsupp, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B13 = matrix(cellcounts), hcenter
			putexcel A13 = "Top Three Quartiles", hcenter
			putexcel A14 = "Lowest Quartile", hcenter
		
	* IABP
		* Female
			tab FEMALE iabp_tot, row matcell(cellcounts)

			levelsof mechsupp_s, local(mechsupplabels)
			matrix colnames cellcounts = `mechsupplabels'
			
			matrix list cellcounts
			
			putexcel F2 = matrix(cellcounts), names hcenter
			putexcel F2 = "IABP", hcenter
			putexcel F3 = "Male", hcenter
			putexcel F4 = "Female", hcenter
			putexcel I2 = "Total", hcenter
			
		* Race
			tab race2 iabp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel G5 = matrix(cellcounts), hcenter
			putexcel F5 = "White", hcenter
			putexcel F6 = "Black", hcenter
			putexcel F7 = "Hispanic", hcenter
			putexcel F8 = "Other Race", hcenter
		
		* Insurance
			tab insurance iabp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel G9 = matrix(cellcounts), hcenter
			putexcel F9 = "Medicare", hcenter
			putexcel F10 = "Medicaid", hcenter
			putexcel F11 = "Private", hcenter
			putexcel F12 = "Uninsured", hcenter
		
		* Income
			tab lowinc iabp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel G13 = matrix(cellcounts), hcenter
			putexcel F13 = "Top Three Quartiles", hcenter
			putexcel F14 = "Lowest Quartile", hcenter
		
	* pVAD
		* Female
			tab FEMALE tand_imp_tot, row matcell(cellcounts)

			levelsof mechsupp_s, local(mechsupplabels)
			matrix colnames cellcounts = `mechsupplabels'
			
			matrix list cellcounts
			
			putexcel K2 = matrix(cellcounts), names hcenter
			putexcel K2 = "pVAD", hcenter
			putexcel K3 = "Male", hcenter
			putexcel K4 = "Female", hcenter
			putexcel N2 = "Total", hcenter
			
		* Race
			tab race2 tand_imp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel L5 = matrix(cellcounts), hcenter
			putexcel K5 = "White", hcenter
			putexcel K6 = "Black", hcenter
			putexcel K7 = "Hispanic", hcenter
			putexcel K8 = "Other Race", hcenter
		
		* Insurance
			tab insurance tand_imp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel L9 = matrix(cellcounts), hcenter
			putexcel K9 = "Medicare", hcenter
			putexcel K10 = "Medicaid", hcenter
			putexcel K11 = "Private", hcenter
			putexcel K12 = "Uninsured", hcenter
		
		* Income
			tab lowinc tand_imp_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel L13 = matrix(cellcounts), hcenter
			putexcel K13 = "Top Three Quartiles", hcenter
			putexcel K14 = "Lowest Quartile", hcenter
		
	* ECMO
		* Female
			tab FEMALE ecmo_tot, row matcell(cellcounts)

			levelsof mechsupp_s, local(mechsupplabels)
			matrix colnames cellcounts = `mechsupplabels'
			
			matrix list cellcounts
			
			putexcel P2 = matrix(cellcounts), names hcenter
			putexcel P2 = "ECMO", hcenter
			putexcel P3 = "Male", hcenter
			putexcel P4 = "Female", hcenter
			putexcel S2 = "Total", hcenter
			
		* Race
			tab race2 ecmo_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel Q5 = matrix(cellcounts), hcenter
			putexcel P5 = "White", hcenter
			putexcel P6 = "Black", hcenter
			putexcel P7 = "Hispanic", hcenter
			putexcel P8 = "Other Race", hcenter
		
		* Insurance
			tab insurance ecmo_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel Q9 = matrix(cellcounts), hcenter
			putexcel P9 = "Medicare", hcenter
			putexcel P10 = "Medicaid", hcenter
			putexcel P11 = "Private", hcenter
			putexcel P12 = "Uninsured", hcenter
		
		* Income
			tab lowinc ecmo_tot, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel Q13 = matrix(cellcounts), hcenter
			putexcel P13 = "Top Three Quartiles", hcenter
			putexcel P14 = "Lowest Quartile", hcenter
		
	* Close log
		log close

///////////////////////////////////////////////////////////////////	

* Figure 2 Mortality Rate 

	* Create log file
		log using "Output\F2 Mortality", replace text
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(F2 Mortality) modify
		putexcel A1 = "Figure 2: Mortality Rate", bold left

		decode DIED, generate(died_s)
		
	* Any MCS
	
		* Female
			tab FEMALE DIED, row matcell(cellcounts)

			levelsof died_s, local(diedlabels)
			matrix colnames cellcounts = `diedlabels'
			
			matrix list cellcounts
			
			putexcel A2 = matrix(cellcounts), names hcenter
			putexcel A2 = "Mortality", hcenter
			putexcel A3 = "Male", hcenter
			putexcel A4 = "Female", hcenter
			putexcel D2 = "Total", hcenter
			
		* Race
			tab race2 DIED, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B5 = matrix(cellcounts), hcenter
			putexcel A5 = "White", hcenter
			putexcel A6 = "Black", hcenter
			putexcel A7 = "Hispanic", hcenter
			putexcel A8 = "Other Race", hcenter
		
		* Insurance
			tab insurance DIED, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B9 = matrix(cellcounts), hcenter
			putexcel A9 = "Medicare", hcenter
			putexcel A10 = "Medicaid", hcenter
			putexcel A11 = "Private", hcenter
			putexcel A12 = "Uninsured", hcenter
		
		* Income
			tab lowinc DIED, row matcell(cellcounts)
			
			matrix list cellcounts
			
			putexcel B13 = matrix(cellcounts), hcenter
			putexcel A13 = "Top Three Quartiles", hcenter
			putexcel A14 = "Lowest Quartile", hcenter
		
	* Close log
		log close

///////////////////////////////////////////////////////////////////
	