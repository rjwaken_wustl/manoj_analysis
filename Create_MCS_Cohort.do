capture log close	// close any open log files
version 15.1 		// make sure version is the same every time it's run
clear all 			// clear any open datasets
macro drop _all 	// drop all macros
set linesize 100 	// set line size for legibility
//ssc install groups	// install groups package if you haven't done so already

* Based on \\storage1.ris.wustl.edu\kjoyntmaddox\Active\Alina\Cardiovascular Race - Gmerice\Stroke Data and Code

* Set current directory
	cd "\\storage1.ris.wustl.edu\kjoyntmaddox\Active\Daniel\MCS Disparities Manoj"

* Create log file	
	log using "Output\Create MCS Cohort", replace text


////// Create Cardiac Shock Dataset //////
	* 2012
		use "Original Data\nis2012.dta"

		*icd9 clean DX
		foreach i of varlist DX1-DX25 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		*gen cardshock, keep cardshock
		icd9 gen cardshock = DX1, range(78551)
		forvalues i = 1/25 {
			replace cardshock = 1 if DX`i' == "78551"
		}
		keep if cardshock == 1
		 
		* save cardshock12
		save "New Data\cardshock12.dta",replace
		clear

	* 2013
		use "Original Data\nis2013.dta"

		*icd9 clean DX
		foreach i of varlist DX1-DX25 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		* gen cardshock, keep cardshock
		icd9 gen cardshock = DX1, range(78551)
		forvalues i = 1/25 {
			replace cardshock = 1 if DX`i' == "78551"
		}
		keep if cardshock == 1
		 
		* save cardshock13
		save "New Data\cardshock13.dta",replace
		clear

	* 2014
		use "Original Data\nis2014.dta"

		*icd9 clean DX
		foreach i of varlist DX1-DX30 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		* gen cardshock, keep cardshock
		icd9 gen cardshock = DX1, range(78551)
		forvalues i = 1/30 {
			replace cardshock = 1 if DX`i' == "78551"
		}
		keep if cardshock == 1
		 
		* save cardshock14
		save "New Data\cardshock14.dta",replace
		clear

	* 2015 Q1Q3
		use "Original Data\nis2015_withcm.dta"

		*icd9 clean DX
		foreach i of varlist DX1-DX30 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}
		
		*icd10 clean DX
		foreach i of varlist I10_DX1-I10_DX30 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		*gen stroke, keep stroke
		icd9 gen cardshock1 = DX1, range(78551)
		forvalues i = 1/30 {
			replace cardshock1 = 1 if DX`i' == "78551"
		}

		icd10 gen cardshock2 = I10_DX1, range(R570)
		forvalues i = 1/30 {
			replace cardshock2 = 1 if I10_DX`i' == "R570"
		}
		
		gen cardshock = 0
		replace cardshock = 1 if cardshock1 == 1
		replace cardshock = 1 if cardshock2 == 1

		keep if cardshock == 1
		 
		*save cardshock15
		save "New Data\cardshock15.dta",replace
		clear


	* 2016
		use "Original Data\nis2016_withcm.dta"

		*icd10 clean DX
		foreach i of varlist I10_DX1-I10_DX30 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		*gen cardshock, keep cardshock
		icd10 gen cardshock = I10_DX1, range(R570)
		forvalues i = 1/30 {
			replace cardshock = 1 if I10_DX`i' == "R570"
		}
		keep if cardshock == 1
		 
		*save cardshock16
		save "New Data\cardshock16.dta",replace
		clear


	* 2017
		use "Original Data\nis2017_cm.dta"

		*icd10 clean DX
		foreach i of varlist I10_DX1-I10_DX40 {
		replace `i' = "" if `i'=="incn"
		replace `i' = "" if `i'=="invl"
		}

		*gen cardshock, keep cardshock
		icd10 gen cardshock = I10_DX1, range(R570)
		forvalues i = 1/40 {
			replace cardshock = 1 if I10_DX`i' == "R570"
		}
		keep if cardshock == 1
		 
		*save cardshock17
		save "New Data\cardshock17.dta",replace
		clear

	*Combine all years
		use "New Data\cardshock12.dta"
		append using "New Data\cardshock13.dta"
		append using "New Data\cardshock14.dta"
		append using "New Data\cardshock15.dta"
		append using "New Data\cardshock16.dta"
		append using "New Data\cardshock17.dta"
		save "New Data\cardshock1217_full.dta",replace

/////////////////////////////////////
* Use all years, data management

use "New Data\cardshock1217_full.dta"

* Group DX and PR codes in dataset
order DX26-DX30, after(DX25)
order I10_DX31-I10_DX40, after(I10_DX30)
order I10_PR16-I10_PR25, after(I10_PR15)

*  Create rural var
gen rur6 = .
replace rur6 = 1 if PL_NCHS==1 | PL_NCHS2006==1
replace rur6 = 2 if PL_NCHS==2 | PL_NCHS2006==2
replace rur6 = 3 if PL_NCHS==3 | PL_NCHS2006==3
replace rur6 = 4 if PL_NCHS==4 | PL_NCHS2006==4
replace rur6 = 5 if PL_NCHS==5 | PL_NCHS2006==5
replace rur6 = 6 if PL_NCHS==6 | PL_NCHS2006==6
replace PL_NCHS = rur6

* Create three-part rural var
gen rur3 = .
replace rur3 = 1 if rur6 == 1
replace rur3 = 1 if rur6 == 2
replace rur3 = 2 if rur6 == 3
replace rur3 = 2 if rur6 == 4
replace rur3 = 3 if rur6 == 5
replace rur3 = 3 if rur6 == 6

* delete all observations for patients < 18 y.o.
keep if AGE >= 18

* drop transfer out
drop if TRAN_OUT == 1

* create transfer in
gen transfer_in = 0 if TRAN_IN ==0 | TRAN_IN == 2
replace transfer_in = 1 if TRAN_IN == 1
tab transfer_in,missing
drop if TRAN_IN == .

* Drop missings
drop if PL_NCHS == .
drop if DISPUNIFORM == .
drop if LOS == .
drop if FEMALE==.
drop if ZIPINC_QRTL==.
drop if TOTCHG == .

* gen dischhome
gen dischhome = 0 
replace dischhome = 1 if DISPUNIFORM == 1 | DISPUNIFORM == 6

/* individual rural
gen rur1 = 0
gen rur2 = 0
gen rur3 = 0
gen rur4 = 0
gen rur5 = 0
gen rur6 = 0
replace rur1 = 1 if PL_NCHS==1
replace rur2 = 1 if PL_NCHS==2
replace rur3 = 1 if PL_NCHS==3
replace rur4 = 1 if PL_NCHS==4
replace rur5 = 1 if PL_NCHS==5
replace rur6 = 1 if PL_NCHS==6 */

* gen race2
gen race2 = RACE
replace race2 = 4 if RACE == 5
replace race2 = 4 if RACE == 6
replace race2 = 4 if RACE == .

* gen agecat
egen agecat = cut(AGE), at(18,35,55,65,75,100)
tab agecat
drop if agecat ==.

* gen insurance
gen insurance = PAY1
replace insurance = 4 if PAY1==5
replace insurance = 5 if PAY1==6
tab insurance PAY1
drop if insurance == .

* gen lowinc
gen lowinc = ZIPINC_QRTL
replace lowinc = 0 if ZIPINC_QRTL==2
replace lowinc = 0 if ZIPINC_QRTL==3
replace lowinc = 0 if ZIPINC_QRTL==4
tab lowinc ZIPINC_QRTL
drop if lowinc == .

* gen HOSP_ID_YEAR
gen HOSP_ID_YEAR = string(HOSP_NIS) + "_" + string(YEAR)

* label vars
label define FEMALE 0 "Male" 1 "Female"
label values FEMALE FEMALE
label define races 1 "White" 2 "Black" 3 "Hispanic" 4 "Other and missing"
label values race2 races
label define ins 1 "Medicare" 2 "Medicaid" 3 "Private" 4 "Uninsured" 5 "Other"
label values insurance ins
label define lowinc_lab 0 "Top Three Quartiles" 1 "Lowest Quartile"
label values lowinc lowinc_lab
label define year_lab 2012 "2012" 2013 "2013" 2014 "2014" 2015 "2015" 2016 "2016" 2017 "2017"
label values YEAR year_lab
label define died_lab 0 "Did not die" 1 "Died"
label values DIED died_lab
label define income 1 "1 - Lowest" 2 "2" 3 "3" 4 "4 - Highest"
label values ZIPINC_QRTL income
label define HOSP_BEDSIZE_lab 1 "Small" 2 "Medium" 3 "Large"
label values HOSP_BEDSIZE HOSP_BEDSIZE_lab
label define H_CONTRL_lab 1 "Government, nonfederal (public)" 2 "Private, not-for-profit (voluntary)" 3 "Private, investor owned (proprietary)"
label values H_CONTRL H_CONTRL_lab
label define HOSP_LOCTEACH_lab 1 "Rural" 2 "Urban - Non-teaching hospital" 3 "Urban - Teaching hospital"
label values HOSP_LOCTEACH HOSP_LOCTEACH_lab
label define reg 1 "NE" 2 "MW" 3 "S" 4 "W"
label values HOSP_REGION reg
label define agecat_lab 18 "18-34" 35 "35-54" 55 "55-64" 65 "65-74" 75 "75+"
label values agecat agecat_lab
label define rur6_lab 1 "Urban" 2 "Suburban" 3 "Medium City" 4 "Small City" 5 "Micropolitan Rural" 6 "Non-Core Rural"
label values rur6 rur6_lab
label define rur3_lab 1 "Urban" 2 "Town" 3 "Rural"
label values rur3 rur3_lab
label define TRAN_IN_lab 0 "Not transferred in" 1 "From hospital" 2 "From other health facility"
label values TRAN_IN TRAN_IN_lab

///////////////////////////////
* Comorbidities
* * gen NSTEMI

foreach i of varlist DX1-DX30 {
	icd9 gen nstemi_`i'=`i',range(410.7*)
}

foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen nstemi_`i'=`i',range(I214*)
}

gen nstemi_tot=0
forvalues i = 1/30 {
    replace nstemi_tot=1 if nstemi_DX`i' == 1
}
forvalues i = 1/40 {
    replace nstemi_tot=1 if nstemi_I10_DX`i' == 1
}

drop nstemi_DX1-nstemi_DX30
drop nstemi_I10_DX1-nstemi_I10_DX40

tab nstemi_tot

* * gen STEMI

foreach i of varlist DX1-DX30 {
	icd9 gen stemi_`i'=`i',range(410.00 410.01 410.1* 410.2* 410.3* 410.4* 410.5* 410.6* 410.8* 410.9*)
}

foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen stemi_`i'=`i',range(I210* I211* I212* I213*)
}

gen stemi_tot=0
forvalues i = 1/30 {
    replace stemi_tot=1 if stemi_DX`i' == 1
}
forvalues i = 1/40 {
    replace stemi_tot=1 if stemi_I10_DX`i' == 1
}

drop stemi_DX1-stemi_DX30
drop stemi_I10_DX1-stemi_I10_DX40

tab stemi_tot

* * gen unstable angina

foreach i of varlist DX1-DX30 {
	icd9 gen unstable_angina_`i'=`i',range(411.1)
}

foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen unstable_angina_`i'=`i',range(I200*)
}

gen unstable_angina_tot=0
forvalues i = 1/30 {
    replace unstable_angina_tot=1 if unstable_angina_DX`i' == 1
}
forvalues i = 1/40 {
    replace unstable_angina_tot=1 if unstable_angina_I10_DX`i' == 1
}

drop unstable_angina_DX1-unstable_angina_DX30
drop unstable_angina_I10_DX1-unstable_angina_I10_DX40

tab unstable_angina_tot

* * gen PCI

foreach i of varlist PR1-PR15 {
	replace `i' = "" if `i'=="incn"
	replace `i' = "" if `i'=="invl"
}

foreach i of varlist I10_PR1-I10_PR25 {
	replace `i' = "" if `i'=="incn"
	replace `i' = "" if `i'=="invl"
}

foreach i of varlist PR1-PR15 {
	icd9p gen pci_`i'=`i',range(36.06 36.07)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen pci_`i'=`i',range(0270046 027004Z 0270056 027005Z 0270066 027006Z 0270076 027007Z 02700D6 02700DZ 02700E6 02700EZ 02700F6 02700FZ 02700G6 02700GZ 02700T6 02700TZ 02700Z6 02700ZZ 0270346 027034Z 0270356 027035Z 0270366 027036Z 0270376 027037Z 02703D6 02703DZ 02703E6 02703EZ 02703F6 02703FZ 02703G6 02703GZ 02703T6 02703TZ 02703Z6 02703ZZ 0271346 027134Z 0271356 027135Z 0271366 027136Z 0271376 027137Z 02713D6 02713DZ 02713E6 02713EZ 02713F6 02713FZ 02713G6 02713GZ 02713T6 02713TZ 02713Z6 02713ZZ 0272346 027234Z 0272356 027235Z 0272366 027236Z 0272376 027237Z 02723D6 02723DZ 02723E6 02723EZ 02723F6 02723FZ 02723G6 02723GZ 02723T6 02723TZ 02723Z6 02723ZZ 0273346 027334Z 0273356 027335Z 0273366 027336Z 0273376 027337Z 02733D6 02733DZ 02733E6 02733EZ 02733F6 02733FZ 02733G6 02733GZ 02733T6 02733TZ 02733Z6 02733ZZ)
}
foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen pci_`i'=`i',range(Z955*)
}

gen pci_tot=0
forvalues i = 1/15 {
    replace pci_tot=1 if pci_PR`i' == 1
}
forvalues i = 1/25 {
    replace pci_tot=1 if pci_I10_PR`i' == 1
}
forvalues i = 1/40 {
    replace pci_tot=1 if pci_I10_DX`i' == 1
}

drop pci_PR1-pci_PR15
drop pci_I10_PR1-pci_I10_PR25
drop pci_I10_DX1-pci_I10_DX40

tab pci_tot

* * gen ischemia

gen ischemia = 0
foreach var in nstemi_tot stemi_tot unstable_angina_tot pci_tot {
	replace ischemia = 1 if `var' == 1
}

tab ischemia

* * gen systolic heart failure

foreach i of varlist DX1-DX30 {
	icd9 gen syshf_`i'=`i',range(428.21 428.23)
}
foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen syshf_`i'=`i',range(I50 I501 I502* I504* I508* I509)
}

gen syshf_tot=0
forvalues i = 1/30 {
    replace syshf_tot=1 if syshf_DX`i' == 1
}
forvalues i = 1/40 {
    replace syshf_tot=1 if syshf_I10_DX`i' == 1
}

drop syshf_DX1-syshf_DX30
drop syshf_I10_DX1-syshf_I10_DX40

tab syshf_tot

* * gen diastolic heart failure

foreach i of varlist DX1-DX30 {
	icd9 gen diahf_`i'=`i',range(428.31 428.33)
}
foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen diahf_`i'=`i',range(I503*)
}

gen diahf_tot=0
forvalues i = 1/30 {
    replace diahf_tot=1 if diahf_DX`i' == 1
}
forvalues i = 1/40 {
    replace diahf_tot=1 if diahf_I10_DX`i' == 1
}

drop diahf_DX1-diahf_DX30
drop diahf_I10_DX1-diahf_I10_DX40

tab diahf_tot

* * gen DX1 myocardial infarction (NSTEMI and STEMI)
	
foreach i of varlist DX1 {
	icd9 gen DX1_MI_`i'=`i',range(410.7* 410.00 410.01 410.1* 410.2* 410.3* 410.4* 410.5* 410.6* 410.8* 410.9*)
}
foreach i of varlist I10_DX1 {
	icd10 gen DX1_MI_`i'=`i',range(I214* I210* I211* I212* I213*)
}

gen DX1_MI_tot=0
replace DX1_MI_tot=1 if DX1_MI_DX1 == 1
replace DX1_MI_tot=1 if DX1_MI_I10_DX1 == 1

drop DX1_MI_DX1
drop DX1_MI_I10_DX1

tab DX1_MI_tot

* * gen DX1 sepsis

foreach i of varlist DX1 {
	icd9 gen DX1_sepsis_`i'=`i',range(995.91 995.92 790.7 785.52)
}
foreach i of varlist I10_DX1 {
	icd10 gen DX1_sepsis_`i'=`i',range(A41*)
}

gen DX1_sepsis_tot=0
replace DX1_sepsis_tot=1 if DX1_sepsis_DX1 == 1
replace DX1_sepsis_tot=1 if DX1_sepsis_I10_DX1 == 1

drop DX1_sepsis_DX1
drop DX1_sepsis_I10_DX1

tab DX1_sepsis_tot

* * gen DX1 overdose

foreach i of varlist DX1 {
	icd10 gen DX1_overdose_`i'=`i',range(965.0*)
}
foreach i of varlist I10_DX1 {
	icd10 gen DX1_overdose_`i'=`i',range(T40*)
}

gen DX1_overdose_tot=0
replace DX1_overdose_tot=1 if DX1_overdose_DX1 == 1
replace DX1_overdose_tot=1 if DX1_overdose_I10_DX1 == 1

drop DX1_overdose_DX1
drop DX1_overdose_I10_DX1

tab DX1_overdose_tot

//////////////////////////
* Procedures

foreach i of varlist PR1-PR15 {
	replace `i' = "" if `i'=="incn"
	replace `i' = "" if `i'=="invl"
}

foreach i of varlist I10_PR1-I10_PR25 {
	replace `i' = "" if `i'=="incn"
	replace `i' = "" if `i'=="invl"
}

* * gen SAVR

foreach i of varlist PR1-PR15 {
	icd9p gen savr_`i'=`i',range(35.21 35.22)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen savr_`i'=`i',range(02RF07Z 02RF08Z 02RF0KZ 02RF47Z  02RF48Z 02RF4JZ 02RF4KZ  02RF0JZ)
}

gen savr_tot=0
forvalues i = 1/15 {
    replace savr_tot=1 if savr_PR`i' == 1
}
forvalues i = 1/25 {
    replace savr_tot=1 if savr_I10_PR`i' == 1
}

drop savr_PR1-savr_PR15
drop savr_I10_PR1-savr_I10_PR25

tab savr_tot

* * gen CABG

foreach i of varlist PR1-PR15 {
	icd9p gen cabg_`i'=`i',range(36.1*)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen cabg_`i'=`i',range(0210* 0211* 0212* 0213*)
}

gen cabg_tot=0
forvalues i = 1/15 {
    replace cabg_tot=1 if cabg_PR`i' == 1
}
forvalues i = 1/25 {
    replace cabg_tot=1 if cabg_I10_PR`i' == 1
}

drop cabg_PR1-cabg_PR15
drop cabg_I10_PR1-cabg_I10_PR25

tab cabg_tot

* * gen MVS (mitral valve surgery)

foreach i of varlist PR1-PR15 {
	icd9p gen mvs_`i'=`i',range(35.23 35.24)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen mvs_`i'=`i',range(02UG07Z 02NG0ZZ 027G04Z 02QG0ZZ 02UG08Z 02UG0JZ 02UG0KZ 025G0ZZ 027G0DZ 027G0ZZ 02BG0ZX 02BG0ZZ 02RG07Z 02RG08Z 02RG0JZ 02RG0KZ 02VG0ZZ 02CG0ZZ 02WG07Z 02WG08Z 02WG0JZ 02WG0KZ)
}

gen mvs_tot=0
forvalues i = 1/15 {
    replace mvs_tot=1 if mvs_PR`i' == 1
} 
forvalues i = 1/25 {
    replace mvs_tot=1 if mvs_I10_PR`i' == 1
} 

drop mvs_PR1-mvs_PR15
drop mvs_I10_PR1-mvs_I10_PR25

tab mvs_tot

* * gen PVS (pulmonary valve surgery)

foreach i of varlist PR1-PR15 {
	icd9p gen pvs_`i'=`i',range(35.25 35.26)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen pvs_`i'=`i',range(02WH0KZ 02WH07Z 02WH0JZ 027H04Z 02RH08Z 02TH0ZZ 02RH0JZ 02RH07Z 02BH0ZZ 02BH0ZX 027H0ZZ 027H0DZ 025H0ZZ 02UH0KZ 02UH0JZ 02UH08Z 02UH07Z 02QH0ZZ 02NH0ZZ)
}

gen pvs_tot=0
forvalues i = 1/15 {
    replace pvs_tot=1 if pvs_PR`i' == 1
}
forvalues i = 1/25 {
    replace pvs_tot=1 if pvs_I10_PR`i' == 1
}

drop pvs_PR1-pvs_PR15
drop pvs_I10_PR1-pvs_I10_PR25

tab pvs_tot

* * gen TVS (tricuspid valve surgery)

foreach i of varlist PR1-PR15 {
	icd9p gen tvs_`i'=`i',range(35.27 35.28)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen tvs_`i'=`i',range(02UJ0KZ 027J04Z 02NJ0ZZ 02QJ0ZZ 02UJ07Z 02UJ08Z 02UJ0JZ 027J0DZ 027J0ZZ 02BJ0ZX 02BJ0ZZ 02RJ07Z 02RJ0JZ 02CJ0ZZ 027J04Z 02RJ08Z 02RJ0KZ 02WJ08Z 02WJ0JZ 02WJ07Z 02WJ0KZ)
}

gen tvs_tot=0
forvalues i = 1/15 {
    replace tvs_tot=1 if tvs_PR`i' == 1
}
forvalues i = 1/25 {
    replace tvs_tot=1 if tvs_I10_PR`i' == 1
}

drop tvs_PR1-tvs_PR15
drop tvs_I10_PR1-tvs_I10_PR25

tab tvs_tot

* * gen AVSC (atrium or ventricular septal closure)

foreach i of varlist PR1-PR15 {
	icd9p gen avsc_`i'=`i',range(35.5* 35.6* 35.7*)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen avsc_`i'=`i',range(02Q5* 02QM*)
}

gen avsc_tot=0
forvalues i = 1/15 {
    replace avsc_tot=1 if avsc_PR`i' == 1
}
forvalues i = 1/25 {
    replace avsc_tot=1 if avsc_I10_PR`i' == 1
}

drop avsc_PR1-avsc_PR15
drop avsc_I10_PR1-avsc_I10_PR25

tab avsc_tot

* * gen TEVAR

foreach i of varlist PR1-PR15 {
	icd9p gen tevar_`i'=`i',range(38.35 38.45 39.57)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen tevar_`i'=`i',range(02RW07Z 02RW08Z 02RW0JZ 02RW0KZ  02RW47Z 02RW48Z 02RW4JZ 02RW4KZ 02RX07Z 02RX08Z 02RX0JZ 02RX0KZ 02RX47Z 02RX48Z 02RX4JZ 02RX4KZ 02UW0JZ 02UX0JZ)
}

gen tevar_tot=0
forvalues i = 1/15 {
    replace tevar_tot=1 if tevar_PR`i' == 1
}
forvalues i = 1/25 {
    replace tevar_tot=1 if tevar_I10_PR`i' == 1
}

drop tevar_PR1-tevar_PR15
drop tevar_I10_PR1-tevar_I10_PR25

tab tevar_tot

* * gen AAA repair (endo or open)

foreach i of varlist PR1-PR15 {
	icd9p gen aaa_`i'=`i',range(38.44 39.71)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen aaa_`i'=`i',range(04R007Z 04R00JZ 04R00KZ 04R047Z 04R04JZ 04R04KZ 04U03JZ 04U04JZ 04V00DZ 04V03DZ 04V04DZ)
}

gen aaa_tot=0
forvalues i = 1/15 {
    replace aaa_tot=1 if aaa_PR`i' == 1
}
forvalues i = 1/25 {
    replace aaa_tot=1 if aaa_I10_PR`i' == 1
}

drop aaa_PR1-aaa_PR15
drop aaa_I10_PR1-aaa_I10_PR25

tab aaa_tot

* * gen heart transplant

foreach i of varlist PR1-PR15 {
	icd9p gen transplant_`i'=`i',range(37.51 33.6)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen transplant_`i'=`i',range(02YA0Z0 02YA0Z1 02YA0Z2 02YA0Z0 02YA0Z1 02YA0Z2 0BYM0Z0 0BYM0Z1 0BYM0Z2)
}

gen transplant_tot=0
forvalues i = 1/15 {
    replace transplant_tot=1 if transplant_PR`i' == 1
}
forvalues i = 1/25 {
    replace transplant_tot=1 if transplant_I10_PR`i' == 1
}

drop transplant_PR1-transplant_PR15
drop transplant_I10_PR1-transplant_I10_PR25

tab transplant_tot

* * gen VAD

foreach i of varlist PR1-PR15 {
	icd9p gen vad_`i'=`i',range(37.66 37.52 37.53 37.54 37.55)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen vad_`i'=`i',range(02RK0JZ 02RL0JZ 02WA0JZ 02HA0QZ 02HA3QZ 02HA4QZ)
}

gen vad_tot=0
forvalues i = 1/15 {
    replace vad_tot=1 if vad_PR`i' == 1
}
forvalues i = 1/25 {
    replace vad_tot=1 if vad_I10_PR`i' == 1
}

drop vad_PR1-vad_PR15
drop vad_I10_PR1-vad_I10_PR25

tab vad_tot

* * gen surgery for pts with any major cardiac surgery except transplant or VAD

gen surgery = 0
foreach var in savr_tot cabg_tot mvs_tot pvs_tot tvs_tot avsc_tot tevar_tot aaa_tot {
	replace surgery = 1 if `var' == 1
}

tab surgery

//////////////////////////
* Mechanical Circulatory Support

* * gen ECMO (extracorporeal membrane oxygenation)

/*foreach i of varlist DX1-DX30 {
	icd9 gen ecmo_`i'=`i',range(V15.87)
}*/
foreach i of varlist PR1-PR15 {
	icd9p gen ecmo_`i'=`i',range(39.65 39.66) //39.61
}
/*foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen ecmo_`i'=`i',range(Z92.81)
}*/
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen ecmo_`i'=`i',range(5A1522F 5A1522G 5A1522H 5A15223) //5A1221Z 
}

gen ecmo_tot=0
/*forvalues i = 1/30 {
    replace ecmo_tot=1 if ecmo_DX`i' == 1
}*/
forvalues i = 1/15 {
    replace ecmo_tot=1 if ecmo_PR`i' == 1
}
/*forvalues i = 1/40 {
    replace ecmo_tot=1 if ecmo_I10_DX`i' == 1
}*/
forvalues i = 1/25 {
    replace ecmo_tot=1 if ecmo_I10_PR`i' == 1
}

//drop ecmo_DX1-ecmo_DX30
drop ecmo_PR1-ecmo_PR15
//drop ecmo_I10_DX1-ecmo_I10_DX40
drop ecmo_I10_PR1-ecmo_I10_PR25

tab ecmo_tot

* * gen IABP (intra-aortic balloon pump)

foreach i of varlist PR1-PR15 {
	icd9p gen iabp_`i'=`i',range(37.61)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen iabp_`i'=`i',range(5A02110 5A02210)
}

gen iabp_tot=0
forvalues i = 1/15 {
    replace iabp_tot=1 if iabp_PR`i' == 1
}
forvalues i = 1/25 {
    replace iabp_tot=1 if iabp_I10_PR`i' == 1
}

drop iabp_PR1-iabp_PR15
drop iabp_I10_PR1-iabp_I10_PR25

tab iabp_tot

* * gen pVAD (Tandem/Impella)

foreach i of varlist DX1-DX30 {
	icd9 gen tand_imp_`i'=`i',range(V43.21)
}
foreach i of varlist PR1-PR15 {
	icd9p gen tand_imp_`i'=`i',range(37.68)
}

foreach i of varlist I10_DX1-I10_DX40 {
	icd10 gen tand_imp_`i'=`i',range(Z95.811)
}
foreach i of varlist I10_PR1-I10_PR25 {
	icd10 gen tand_imp_`i'=`i',range(5A0211D 5A02116 5A0221D 5A02216) 
}

gen tand_imp_tot=0
forvalues i = 1/30 {
    replace tand_imp_tot=1 if tand_imp_DX`i' == 1
}
forvalues i = 1/15 {
    replace tand_imp_tot=1 if tand_imp_PR`i' == 1
}
forvalues i = 1/40 {
    replace tand_imp_tot=1 if tand_imp_I10_DX`i' == 1
}
forvalues i = 1/25 {
    replace tand_imp_tot=1 if tand_imp_I10_PR`i' == 1
}

drop tand_imp_DX1-tand_imp_DX30
drop tand_imp_PR1-tand_imp_PR15
drop tand_imp_I10_DX1-tand_imp_I10_DX40
drop tand_imp_I10_PR1-tand_imp_I10_PR25

tab tand_imp_tot

* * gen mechsupport

gen mechsupp = 0
foreach var in ecmo_tot iabp_tot tand_imp_tot {
	replace mechsupp = 1 if `var' == 1
}

tab mechsupp

gen mechsupp_noe = 0
foreach var in iabp_tot tand_imp_tot {
	replace mechsupp_noe = 1 if `var' == 1
}
replace mechsupp_noe = 0 if ecmo_tot == 1

tab mechsupp_noe

*save full
save,replace

//////////////////////////
* Label groups, comorbidities, and procedures

* * Groups
label define ischemia_lab 0 "Group 2" 1 "Group 1"
label values ischemia ischemia_lab

* * Comorbidities
label define cm_pr_lab 0 "No" 1 "Yes"

label values CM_LYTES cm_pr_lab
label values CM_HTN_C cm_pr_lab
label values CM_RENLFAIL cm_pr_lab
label values CM_CHRNLUNG cm_pr_lab
label values CM_DM cm_pr_lab
label values CM_CHF cm_pr_lab
label values CM_ANEMDEF cm_pr_lab
label values CM_COAG cm_pr_lab
label values CM_OBESE cm_pr_lab
label values CM_PERIVASC cm_pr_lab
label values CM_DMCX cm_pr_lab
label values CM_NEURO cm_pr_lab
label values CM_DEPRESS cm_pr_lab
label values CM_VALVE cm_pr_lab
label values CM_PULMCIRC cm_pr_lab
label values CM_ALCOHOL cm_pr_lab
label values CM_LIVER cm_pr_lab
label values CM_DRUG cm_pr_lab

* * Procedures
label values pci_tot cm_pr_lab
label values cabg_tot cm_pr_lab
label values iabp_tot cm_pr_lab
label values ecmo_tot cm_pr_lab
label values tand_imp_tot cm_pr_lab

* * Mechanical support
label values mechsupp cm_pr_lab
label values mechsupp_noe cm_pr_lab

*save full
save,replace

//////////////////////////
* Exclude pts with major cardiac surgery except transplant or VAD

use "New Data\cardshock1217_full.dta"
drop if surgery == 1
save "New Data\cardshock1217_nosurgery.dta",replace
clear

use "New Data\cardshock1217_full.dta"
keep if surgery == 1
drop if DX1_sepsis_tot == 1
drop if DX1_overdose_tot == 1
save "New Data\cardshock1217_surgery.dta",replace
clear

////////////////////////////
* Drop pts with a primary diagnosis of sepsis or overdose

use "New Data\cardshock1217_nosurgery.dta"
drop if DX1_sepsis_tot == 1
drop if DX1_overdose_tot == 1
save "New Data\cardshock1217.dta",replace
clear

////////////////////////////
* Create group 1 (ischemia) and group 2 (no ischemia) datasets

* create group 1 (ischemia)
use "New Data\cardshock1217.dta"
keep if ischemia == 1
save "New Data\cardshock1217_ischemia.dta",replace
clear

* create group 2 (no ischemia)
use "New Data\cardshock1217.dta"
keep if ischemia == 0
save "New Data\cardshock1217_noischemia.dta",replace
clear

log close
