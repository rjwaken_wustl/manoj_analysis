capture log close
version 15.1
clear all
macro drop _all
set linesize 100

* Set current directory
	cd "\\storage1.ris.wustl.edu\kjoyntmaddox\Active\Daniel\MCS Disparities Manoj"

//////////////////////////////////////////////////////////////////////
		
* Supplemental Table 1 Admission Characteristics by Type of MCS
	use "New Data\cardshock1217.dta"
	
	local tot_cols = 2
	local tot_rows = 3

	putexcel set "Output\excelresults.xlsx", sheet(supptable1) modify
	/*tab ischemia, matcell(cellcounts)
	local ColCount = r(r)
	local TotalCount = r(N)
	local Cell = char(64 + `tot_cols') + string(`tot_rows')
	putexcel `Cell' = `TotalCount'
	forvalues col = 1/`ColCount' {
		local cellcount = cellcounts[`col',1]
		local cellpercent = string(100*`cellcount'/`TotalCount',"%9.1f")
		local CellContents = "`cellcount' (`cellpercent'%)"
		local Cell = char(64 + `tot_cols' + `col' - 1) + string(3)
		putexcel `Cell' = "`CellContents'", right
	}*/

	foreach ColVar in mechsupp tand_imp_tot iabp_tot ecmo_tot {
		foreach RowVar in agecat FEMALE race2 insurance lowinc CM_LYTES CM_HTN_C CM_RENLFAIL CM_CHRNLUNG CM_DM CM_CHF CM_ANEMDEF CM_COAG CM_OBESE CM_PERIVASC CM_DMCX CM_NEURO CM_DEPRESS CM_VALVE CM_PULMCIRC CM_ALCOHOL CM_LIVER CM_DRUG {
			
			/*if `RowVar' == TOTCHG {
				local binary 0
				su TOTCHG if ischemia == 0
				local MeanChargesAll2 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 1
				local MeanChargesAll1 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 0 & mechsupp == 1
				local MeanChargesMCS2 = round(r(mean), 0.01)
				su TOTCHG if ischemia == 1 & mechsupp == 1	
				local MeanChargesMCS1 = round(r(mean), 0.01)
				matrix cellcounts = (`MeanChargesAll2', `MeanChargesAll1' \ `MeanChargesMCS2', `MeanChargesMCS1')
				local RowCount = 2
				local ColCount = 2
			}
			else {*/
				quietly tab `RowVar' if !missing(`ColVar'), matcell(rowtotals)
				local binaryrow = r(r) - 1
				quietly tab `ColVar' if !missing(`RowVar'), matcell(coltotals)
				local binarycol = r(r) - 1
				if `tot_cols' == 2 {
				    local binarycol = 2
				}
				quietly tab `RowVar' `ColVar', chi2
				local PVal = r(p)
				local TotalCount = r(N)
				
				if `binaryrow' == 1 { 
					/*if `RowVar' == DX1_MI_tot | `RowVar' == pci_tot {
						tab `RowVar' `ColVar', matcell(fullcellcounts) chi2
						matrix cellcounts = (fullcellcounts[2,1], fullcellcounts[2,2])
						local RowCount = 1
						local ColCount = 2
					}
					else {*/
					if `binarycol' == 1 {
					    tab `RowVar' `ColVar' if `RowVar' == 1 & `ColVar' == 1, matcell(cellcounts) chi2
					}
					else {
						tab `RowVar' `ColVar' if `RowVar' == 1, matcell(cellcounts) chi2
					}
						//local RowCount = r(r)
						//local ColCount = r(c)
					//}
				}
				else {
				    if `binarycol' == 1 {
					    tab `RowVar' `ColVar' if `ColVar' == 1, matcell(cellcounts) chi2
					}
					else {
						tab `RowVar' `ColVar', matcell(cellcounts) chi2
						local PVal = r(p)
					}
					//local RowCount = r(r)
					//local ColCount = r(c)
				}
				local RowCount = r(r)
				local ColCount = r(c)
				
				local RowValueLabel : value label `RowVar'
				levelsof `RowVar', local(RowLevels)
				 
				local ColValueLabel : value label `ColVar'
				levelsof `ColVar', local(ColLevels)
			//}
			
			putexcel set "Output\excelresults.xlsx", sheet(supptable1) modify
			
			if `tot_cols' == 2 {
				local Cell = char(64 + 1) + string(1 + `tot_rows')
				putexcel `Cell' = "`RowVar'", left
			}
			if `tot_rows' == 3 {
				local Cell = char(64 + `tot_cols') + string(2)
				putexcel `Cell' = "`RowVar'", left
			}	
			
			forvalues row = 1/`RowCount' {
			    if `tot_cols' == 2 {
					if `binaryrow' == 1 {
						local cellcount = rowtotals[2,1]
						local Cell = char(64 + `tot_cols') + string(`row' + `tot_rows')
					}
					else {
						local RowValueLabelNum = word("`RowLevels'", `row')
						local Cell = char(64 + 1) + string(`row' + 1 + `tot_rows')
						/*if `RowVar' == TOTCHG {
							if `row' == 1 {
								local CellContents = "Cardiogenic Shock Admissions"
							}
							if `row' == 2 {
								local CellContents = "Only Admissions with MCS"
							}
						}
						else {*/
							local CellContents : label `RowValueLabel' `RowValueLabelNum'
						//}
						putexcel `Cell' = "`CellContents'", right
					}
				}
				
				forvalues col = 1/`ColCount' {
					local cellcount = cellcounts[`row',`col']
					
					/*if `RowVar' == TOTCHG {
						local CellContents = "$`cellcount'"
					}
					else {*/
						local cellpercent = string(100*`cellcount'/coltotals[`col',1],"%9.1f")
						local CellContents = "`cellcount' (`cellpercent'%)"
					//}
					
					if `binaryrow' == 1 {
						local Cell = char(64 + `tot_cols' + `col' - 1) + string(`row' + `tot_rows')
					}
					else {
						local Cell = char(64 + `tot_cols' + `col' - 1) + string(`row' + 1 + `tot_rows')
					}
					
					putexcel `Cell' = "`CellContents'", right
					
					if `row'==1 {
						local Cell = char(64 + `tot_cols' + `col' - 1) + string(2)
						putexcel `Cell' = "`ColVar'", hcenter
						local ColValueLabelNum = word("`ColLevels'", `col')
						local CellContents : label `ColValueLabel' `ColValueLabelNum'
						local Cell = char(64 + `tot_cols' + `col' - 1) + string(3)
						putexcel `Cell' = "`CellContents'", hcenter
					}
				}
				if `tot_cols' == 2 {
					local Cell = char(64 + `tot_cols' + 2) + string(`tot_rows' + 1)
					putexcel `Cell' = `PVal', right
				}
			}
			
			if `binaryrow' == 1 {
				local tot_rows = `tot_rows' + `RowCount'
			}
			else {
				local tot_rows = `tot_rows' + `RowCount' + 1
			}
			
			/*if `tot_rows' == 30 {
				local tot_rows = `tot_rows' + 1 //For "Clinical Comorbidities" label
			}*/
		}
		if `binarycol' == 1 {
			local tot_cols = `tot_cols' + `ColCount'
		}
		else {
			local tot_cols = `tot_cols' + `ColCount' + 1
		}
		//local tot_cols = `tot_cols' + `ColCount'
		local tot_rows = 3
	}
	
//////////////////////////////////////////////////////////////////////

* Supplemental Table 2 Surgical Procedures

	* Create log file
		log using "Output\Supplemental Surgical Procedures", replace text

	* Set excel sheet 
		putexcel set "Output\excelresults.xlsx", sheet(ST2 Procedures) modify
		tab savr_tot, matcell(cellcounts)
		
	* Use dataset
		clear
		use "New Data\cardshock1217_surgery.dta"

	* gen shortened procedure codes
		gen PR1_short = substr(PR1,1,3)
		gen PR1_short2 = substr(PR1,1,4)
		gen I10_PR1_short = substr(I10_PR1,1,4)

	* Tabulate most common 
		groups PR1, order(h) select(10)
		groups I10_PR1, order(h) select(10)
		
		groups PR1_short, order(h) select(10)
		groups PR1_short2, order(h) select(10)
		groups I10_PR1_short, order(h) select(10)
		
	* Tabulate procedures
		tab surgery
		tab savr_tot		
		tab cabg_tot
		tab mvs_tot 
		tab pvs_tot 
		tab tvs_tot 
		tab avsc_tot 
		tab tevar_tot 
		tab aaa_tot

	* Exclude procedures in order
		drop if savr_tot == 1
		drop if cabg_tot == 1
		drop if mvs_tot == 1
		drop if pvs_tot == 1
		drop if tvs_tot == 1
		drop if avsc_tot == 1
		drop if tevar_tot == 1
		drop if aaa_tot == 1
		
	* Clear
		clear
		
	* Close log
		log close
		
///////////////////////////////////////////////////////////////////	

* Supplemental Figure 1A Total Admissions for Cardiogenic Shock and Use of Mechanical Circulatory Support

	* Create log file
		log using "Output\F1A Admissions", replace text
	
	* Use dataset
		clear
		use "New Data\cardshock1217.dta"
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(F1A Admissions) modify
		putexcel A1 = "Figure 1A: Total Admissions for Cardiogenic Shock and Use of Mechanical Circulatory Support", bold left
		
		decode YEAR, generate(year_s)
		decode mechsupp, generate(mechsupp_s)
		
	* Total Admissions
		tab YEAR, matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		matrix list cellcounts
		
		putexcel A2 = matrix(cellcounts), names hcenter
		putexcel B2 = "Total Admissions"

		putexcel A9 = "Total", hcenter
		putexcel B9 = formula(=SUM(B3:B8)), hcenter
		
	* IABP
		tab YEAR iabp_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel F2 = matrix(cellcounts), names hcenter
		putexcel F2 = "IABP"
		
		putexcel I2 = "Total", hcenter
		putexcel F9 = "Total", hcenter
		
		putexcel I3 = formula(=SUM(G3:H3)), hcenter
		putexcel I4 = formula(=SUM(G4:H4)), hcenter
		putexcel I5 = formula(=SUM(G5:H5)), hcenter
		putexcel I6 = formula(=SUM(G6:H6)), hcenter
		putexcel I7 = formula(=SUM(G7:H7)), hcenter
		putexcel I8 = formula(=SUM(G8:H8)), hcenter
		putexcel G9 = formula(=SUM(G3:G8)), hcenter
		putexcel H9 = formula(=SUM(H3:H8)), hcenter
		putexcel I9 = formula(=SUM(G3:H8)), hcenter
		
	* pVAD
		tab YEAR tand_imp_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel K2 = matrix(cellcounts), names hcenter
		putexcel K2 = "pVAD"
		
		putexcel N2 = "Total", hcenter
		putexcel K9 = "Total", hcenter
		
		putexcel N3 = formula(=SUM(L3:M3)), hcenter
		putexcel N4 = formula(=SUM(L4:M4)), hcenter
		putexcel N5 = formula(=SUM(L5:M5)), hcenter
		putexcel N6 = formula(=SUM(L6:M6)), hcenter
		putexcel N7 = formula(=SUM(L7:M7)), hcenter
		putexcel N8 = formula(=SUM(L8:M8)), hcenter
		putexcel L9 = formula(=SUM(L3:L8)), hcenter
		putexcel M9 = formula(=SUM(M3:M8)), hcenter
		putexcel N9 = formula(=SUM(L3:M8)), hcenter
		
	* ECMO
		tab YEAR ecmo_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel P2 = matrix(cellcounts), names hcenter
		putexcel P2 = "ECMO"
		
		putexcel S2 = "Total", hcenter
		putexcel P9 = "Total", hcenter
		
		putexcel S3 = formula(=SUM(Q3:R3)), hcenter
		putexcel S4 = formula(=SUM(Q4:R4)), hcenter
		putexcel S5 = formula(=SUM(Q5:R5)), hcenter
		putexcel S6 = formula(=SUM(Q6:R6)), hcenter
		putexcel S7 = formula(=SUM(Q7:R7)), hcenter
		putexcel S8 = formula(=SUM(Q8:R8)), hcenter
		putexcel Q9 = formula(=SUM(Q3:Q8)), hcenter
		putexcel R9 = formula(=SUM(R3:R8)), hcenter
		putexcel S9 = formula(=SUM(Q3:R8)), hcenter
		
	* Close log
		log close

///////////////////////////////////////////////////////////////////	

* Supplemental Figure 1B Rate of Use of Mechanical Circulatory Support

	* Create log file
		log using "Output\F1B MCS Use", replace text
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(F1B MCS Use) modify
		putexcel A1 = "Figure 1B: Rate of Use of Mechanical Circulatory Support", bold left
		
	* Any MCS
		tab YEAR mechsupp, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel A2 = matrix(cellcounts), names hcenter
		putexcel A2 = "Any MCS"
		
		putexcel D2 = "Total", hcenter
		putexcel A9 = "Total", hcenter
		
		putexcel D3 = formula(=SUM(B3:C3)), hcenter
		putexcel D4 = formula(=SUM(B4:C4)), hcenter
		putexcel D5 = formula(=SUM(B5:C5)), hcenter
		putexcel D6 = formula(=SUM(B6:C6)), hcenter
		putexcel D7 = formula(=SUM(B7:C7)), hcenter
		putexcel D8 = formula(=SUM(B8:C8)), hcenter
		putexcel B9 = formula(=SUM(B3:B8)), hcenter
		putexcel C9 = formula(=SUM(C3:C8)), hcenter
		putexcel D9 = formula(=SUM(B3:C8)), hcenter
		
	* IABP
		tab YEAR iabp_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel F2 = matrix(cellcounts), names hcenter
		putexcel F2 = "IABP"
		
		putexcel I2 = "Total", hcenter
		putexcel F9 = "Total", hcenter
		
		putexcel I3 = formula(=SUM(G3:H3)), hcenter
		putexcel I4 = formula(=SUM(G4:H4)), hcenter
		putexcel I5 = formula(=SUM(G5:H5)), hcenter
		putexcel I6 = formula(=SUM(G6:H6)), hcenter
		putexcel I7 = formula(=SUM(G7:H7)), hcenter
		putexcel I8 = formula(=SUM(G8:H8)), hcenter
		putexcel G9 = formula(=SUM(G3:G8)), hcenter
		putexcel H9 = formula(=SUM(H3:H8)), hcenter
		putexcel I9 = formula(=SUM(G3:H8)), hcenter
		
	* pVAD
		tab YEAR tand_imp_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel K2 = matrix(cellcounts), names hcenter
		putexcel K2 = "pVAD"
		
		putexcel N2 = "Total", hcenter
		putexcel K9 = "Total", hcenter
		
		putexcel N3 = formula(=SUM(L3:M3)), hcenter
		putexcel N4 = formula(=SUM(L4:M4)), hcenter
		putexcel N5 = formula(=SUM(L5:M5)), hcenter
		putexcel N6 = formula(=SUM(L6:M6)), hcenter
		putexcel N7 = formula(=SUM(L7:M7)), hcenter
		putexcel N8 = formula(=SUM(L8:M8)), hcenter
		putexcel L9 = formula(=SUM(L3:L8)), hcenter
		putexcel M9 = formula(=SUM(M3:M8)), hcenter
		putexcel N9 = formula(=SUM(L3:M8)), hcenter
		
	* ECMO
		tab YEAR ecmo_tot, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof mechsupp_s, local(mechsupplabels)
		matrix colnames cellcounts = `mechsupplabels'
		
		matrix list cellcounts
		
		putexcel P2 = matrix(cellcounts), names hcenter
		putexcel P2 = "ECMO"
		
		putexcel S2 = "Total", hcenter
		putexcel P9 = "Total", hcenter
		
		putexcel S3 = formula(=SUM(Q3:R3)), hcenter
		putexcel S4 = formula(=SUM(Q4:R4)), hcenter
		putexcel S5 = formula(=SUM(Q5:R5)), hcenter
		putexcel S6 = formula(=SUM(Q6:R6)), hcenter
		putexcel S7 = formula(=SUM(Q7:R7)), hcenter
		putexcel S8 = formula(=SUM(Q8:R8)), hcenter
		putexcel Q9 = formula(=SUM(Q3:Q8)), hcenter
		putexcel R9 = formula(=SUM(R3:R8)), hcenter
		putexcel S9 = formula(=SUM(Q3:R8)), hcenter
		
	* Close log
		log close

///////////////////////////////////////////////////////////////////	

* Supplemental Figure 1C Mortality Rate 

	* Create log file
		log using "Output\F1C Mortality", replace text
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(F1C Mortality) modify
		putexcel A1 = "Figure 1C: Mortality Rate", bold left

		decode DIED, generate(died_s)
		
	* Overall
		tab YEAR DIED, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel A2 = matrix(cellcounts), names hcenter
		putexcel A2 = "Overall"
		
		putexcel D2 = "Total", hcenter
		putexcel A9 = "Total", hcenter
		
		putexcel D3 = formula(=SUM(B3:C3)), hcenter
		putexcel D4 = formula(=SUM(B4:C4)), hcenter
		putexcel D5 = formula(=SUM(B5:C5)), hcenter
		putexcel D6 = formula(=SUM(B6:C6)), hcenter
		putexcel D7 = formula(=SUM(B7:C7)), hcenter
		putexcel D8 = formula(=SUM(B8:C8)), hcenter
		putexcel B9 = formula(=SUM(B3:B8)), hcenter
		putexcel C9 = formula(=SUM(C3:C8)), hcenter
		putexcel D9 = formula(=SUM(B3:C8)), hcenter
		
	* Any MCS
		tab YEAR DIED if mechsupp == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel F2 = matrix(cellcounts), names hcenter
		putexcel F2 = "Any MCS"
		
		putexcel I2 = "Total", hcenter
		putexcel F9 = "Total", hcenter
		
		putexcel I3 = formula(=SUM(G3:H3)), hcenter
		putexcel I4 = formula(=SUM(G4:H4)), hcenter
		putexcel I5 = formula(=SUM(G5:H5)), hcenter
		putexcel I6 = formula(=SUM(G6:H6)), hcenter
		putexcel I7 = formula(=SUM(G7:H7)), hcenter
		putexcel I8 = formula(=SUM(G8:H8)), hcenter
		putexcel G9 = formula(=SUM(G3:G8)), hcenter
		putexcel H9 = formula(=SUM(H3:H8)), hcenter
		putexcel I9 = formula(=SUM(G3:H8)), hcenter
		
	* No MCS
		tab YEAR DIED if mechsupp == 0, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel K2 = matrix(cellcounts), names hcenter
		putexcel K2 = "No MCS"
		
		putexcel N2 = "Total", hcenter
		putexcel K9 = "Total", hcenter
		
		putexcel N3 = formula(=SUM(L3:M3)), hcenter
		putexcel N4 = formula(=SUM(L4:M4)), hcenter
		putexcel N5 = formula(=SUM(L5:M5)), hcenter
		putexcel N6 = formula(=SUM(L6:M6)), hcenter
		putexcel N7 = formula(=SUM(L7:M7)), hcenter
		putexcel N8 = formula(=SUM(L8:M8)), hcenter
		putexcel L9 = formula(=SUM(L3:L8)), hcenter
		putexcel M9 = formula(=SUM(M3:M8)), hcenter
		putexcel N9 = formula(=SUM(L3:M8)), hcenter
		
	* IABP
		tab YEAR DIED if iabp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel P2 = matrix(cellcounts), names hcenter
		putexcel P2 = "IABP"
		
		putexcel S2 = "Total", hcenter
		putexcel P9 = "Total", hcenter
		
		putexcel S3 = formula(=SUM(Q3:R3)), hcenter
		putexcel S4 = formula(=SUM(Q4:R4)), hcenter
		putexcel S5 = formula(=SUM(Q5:R5)), hcenter
		putexcel S6 = formula(=SUM(Q6:R6)), hcenter
		putexcel S7 = formula(=SUM(Q7:R7)), hcenter
		putexcel S8 = formula(=SUM(Q8:R8)), hcenter
		putexcel Q9 = formula(=SUM(Q3:Q8)), hcenter
		putexcel R9 = formula(=SUM(R3:R8)), hcenter
		putexcel S9 = formula(=SUM(Q3:R8)), hcenter
		
	* pVAD
		tab YEAR DIED if tand_imp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel U2 = matrix(cellcounts), names hcenter
		putexcel U2 = "pVAD"
		
		putexcel X2 = "Total", hcenter
		putexcel U9 = "Total", hcenter
		
		putexcel X3 = formula(=SUM(V3:W3)), hcenter
		putexcel X4 = formula(=SUM(V4:W4)), hcenter
		putexcel X5 = formula(=SUM(V5:W5)), hcenter
		putexcel X6 = formula(=SUM(V6:W6)), hcenter
		putexcel X7 = formula(=SUM(V7:W7)), hcenter
		putexcel X8 = formula(=SUM(V8:W8)), hcenter
		putexcel V9 = formula(=SUM(V3:V8)), hcenter
		putexcel W9 = formula(=SUM(W3:W8)), hcenter
		putexcel X9 = formula(=SUM(V3:W8)), hcenter
		
	* ECMO
		tab YEAR DIED if ecmo_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel Z2 = matrix(cellcounts), names hcenter
		putexcel Z2 = "ECMO"
		
		putexcel AC2 = "Total", hcenter
		putexcel Z9 = "Total", hcenter
		
		putexcel AC3 = formula(=SUM(AA3:AB3)), hcenter
		putexcel AC4 = formula(=SUM(AA4:AB4)), hcenter
		putexcel AC5 = formula(=SUM(AA5:AB5)), hcenter
		putexcel AC6 = formula(=SUM(AA6:AB6)), hcenter
		putexcel AC7 = formula(=SUM(AA7:AB7)), hcenter
		putexcel AC8 = formula(=SUM(AA8:AB8)), hcenter
		putexcel AA9 = formula(=SUM(AA3:AA8)), hcenter
		putexcel AB9 = formula(=SUM(AB3:AB8)), hcenter
		putexcel AC9 = formula(=SUM(AA3:AB8)), hcenter
		
	* Close log
		log close
	
//////////////////////////////////////////////////////////////////////

* Supplemental Figure XX Mortality Rate

	* Create log file
		log using "Output\Supplemental Mort Rate - All", replace text
	
	* Use dataset
		clear
		use "New Data\cardshock1217_full.dta"
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(suppfig_mort_all) modify
		putexcel A1 = "Supplemental Figure XX: Mortality Rate for All Cardiogenic Shock Patients", bold left

		
		decode YEAR, generate(year_s)
		decode DIED, generate(died_s)
		
	* Overall
		tab YEAR DIED, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel A2 = matrix(cellcounts), names hcenter
		putexcel A2 = "Overall"
		
		putexcel D2 = "Total", hcenter
		putexcel A9 = "Total", hcenter
		
		putexcel D3 = formula(=SUM(B3:C3)), hcenter
		putexcel D4 = formula(=SUM(B4:C4)), hcenter
		putexcel D5 = formula(=SUM(B5:C5)), hcenter
		putexcel D6 = formula(=SUM(B6:C6)), hcenter
		putexcel D7 = formula(=SUM(B7:C7)), hcenter
		putexcel D8 = formula(=SUM(B8:C8)), hcenter
		putexcel B9 = formula(=SUM(B3:B8)), hcenter
		putexcel C9 = formula(=SUM(C3:C8)), hcenter
		putexcel D9 = formula(=SUM(B3:C8)), hcenter
		
	* Any MCS
		tab YEAR DIED if mechsupp == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel F2 = matrix(cellcounts), names hcenter
		putexcel F2 = "Any MCS"
		
		putexcel I2 = "Total", hcenter
		putexcel F9 = "Total", hcenter
		
		putexcel I3 = formula(=SUM(G3:H3)), hcenter
		putexcel I4 = formula(=SUM(G4:H4)), hcenter
		putexcel I5 = formula(=SUM(G5:H5)), hcenter
		putexcel I6 = formula(=SUM(G6:H6)), hcenter
		putexcel I7 = formula(=SUM(G7:H7)), hcenter
		putexcel I8 = formula(=SUM(G8:H8)), hcenter
		putexcel G9 = formula(=SUM(G3:G8)), hcenter
		putexcel H9 = formula(=SUM(H3:H8)), hcenter
		putexcel I9 = formula(=SUM(G3:H8)), hcenter
		
	* No MCS
		tab YEAR DIED if mechsupp == 0, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel K2 = matrix(cellcounts), names hcenter
		putexcel K2 = "No MCS"
		
		putexcel N2 = "Total", hcenter
		putexcel K9 = "Total", hcenter
		
		putexcel N3 = formula(=SUM(L3:M3)), hcenter
		putexcel N4 = formula(=SUM(L4:M4)), hcenter
		putexcel N5 = formula(=SUM(L5:M5)), hcenter
		putexcel N6 = formula(=SUM(L6:M6)), hcenter
		putexcel N7 = formula(=SUM(L7:M7)), hcenter
		putexcel N8 = formula(=SUM(L8:M8)), hcenter
		putexcel L9 = formula(=SUM(L3:L8)), hcenter
		putexcel M9 = formula(=SUM(M3:M8)), hcenter
		putexcel N9 = formula(=SUM(L3:M8)), hcenter
		
	* IABP
		tab YEAR DIED if iabp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel P2 = matrix(cellcounts), names hcenter
		putexcel P2 = "IABP"
		
		putexcel S2 = "Total", hcenter
		putexcel P9 = "Total", hcenter
		
		putexcel S3 = formula(=SUM(Q3:R3)), hcenter
		putexcel S4 = formula(=SUM(Q4:R4)), hcenter
		putexcel S5 = formula(=SUM(Q5:R5)), hcenter
		putexcel S6 = formula(=SUM(Q6:R6)), hcenter
		putexcel S7 = formula(=SUM(Q7:R7)), hcenter
		putexcel S8 = formula(=SUM(Q8:R8)), hcenter
		putexcel Q9 = formula(=SUM(Q3:Q8)), hcenter
		putexcel R9 = formula(=SUM(R3:R8)), hcenter
		putexcel S9 = formula(=SUM(Q3:R8)), hcenter
		
	* pVAD
		tab YEAR DIED if tand_imp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel U2 = matrix(cellcounts), names hcenter
		putexcel U2 = "pVAD"
		
		putexcel X2 = "Total", hcenter
		putexcel U9 = "Total", hcenter
		
		putexcel X3 = formula(=SUM(V3:W3)), hcenter
		putexcel X4 = formula(=SUM(V4:W4)), hcenter
		putexcel X5 = formula(=SUM(V5:W5)), hcenter
		putexcel X6 = formula(=SUM(V6:W6)), hcenter
		putexcel X7 = formula(=SUM(V7:W7)), hcenter
		putexcel X8 = formula(=SUM(V8:W8)), hcenter
		putexcel V9 = formula(=SUM(V3:V8)), hcenter
		putexcel W9 = formula(=SUM(W3:W8)), hcenter
		putexcel X9 = formula(=SUM(V3:W8)), hcenter
		
	* ECMO
		tab YEAR DIED if ecmo_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel Z2 = matrix(cellcounts), names hcenter
		putexcel Z2 = "ECMO"
		
		putexcel AC2 = "Total", hcenter
		putexcel Z9 = "Total", hcenter
		
		putexcel AC3 = formula(=SUM(AA3:AB3)), hcenter
		putexcel AC4 = formula(=SUM(AA4:AB4)), hcenter
		putexcel AC5 = formula(=SUM(AA5:AB5)), hcenter
		putexcel AC6 = formula(=SUM(AA6:AB6)), hcenter
		putexcel AC7 = formula(=SUM(AA7:AB7)), hcenter
		putexcel AC8 = formula(=SUM(AA8:AB8)), hcenter
		putexcel AA9 = formula(=SUM(AA3:AA8)), hcenter
		putexcel AB9 = formula(=SUM(AB3:AB8)), hcenter
		putexcel AC9 = formula(=SUM(AA3:AB8)), hcenter
		
	* Close log
		log close
		
//////////////////////////////////////////////////////////////////////

* Supplemental Figure XX Mortality Rate

	* Create log file
		log using "Output\Supplemental Mort Rate - Surgery Patients", replace text
	
	* Use dataset
		clear
		use "New Data\cardshock1217_surgery.dta"
	
	* Labels
		putexcel set "Output\excelresults.xlsx", sheet(suppfig_mort_surg) modify
		putexcel A1 = "Supplemental Figure XX: Mortality Rate for Surgery Patients", bold left
		
		decode YEAR, generate(year_s)
		decode DIED, generate(died_s)
		
	* Overall
		tab YEAR DIED, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel A2 = matrix(cellcounts), names hcenter
		putexcel A2 = "Overall"
		
		putexcel D2 = "Total", hcenter
		putexcel A9 = "Total", hcenter
		
		putexcel D3 = formula(=SUM(B3:C3)), hcenter
		putexcel D4 = formula(=SUM(B4:C4)), hcenter
		putexcel D5 = formula(=SUM(B5:C5)), hcenter
		putexcel D6 = formula(=SUM(B6:C6)), hcenter
		putexcel D7 = formula(=SUM(B7:C7)), hcenter
		putexcel D8 = formula(=SUM(B8:C8)), hcenter
		putexcel B9 = formula(=SUM(B3:B8)), hcenter
		putexcel C9 = formula(=SUM(C3:C8)), hcenter
		putexcel D9 = formula(=SUM(B3:C8)), hcenter
		
	* Any MCS
		tab YEAR DIED if mechsupp == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel F2 = matrix(cellcounts), names hcenter
		putexcel F2 = "Any MCS"
		
		putexcel I2 = "Total", hcenter
		putexcel F9 = "Total", hcenter
		
		putexcel I3 = formula(=SUM(G3:H3)), hcenter
		putexcel I4 = formula(=SUM(G4:H4)), hcenter
		putexcel I5 = formula(=SUM(G5:H5)), hcenter
		putexcel I6 = formula(=SUM(G6:H6)), hcenter
		putexcel I7 = formula(=SUM(G7:H7)), hcenter
		putexcel I8 = formula(=SUM(G8:H8)), hcenter
		putexcel G9 = formula(=SUM(G3:G8)), hcenter
		putexcel H9 = formula(=SUM(H3:H8)), hcenter
		putexcel I9 = formula(=SUM(G3:H8)), hcenter
		
	* No MCS
		tab YEAR DIED if mechsupp == 0, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'

		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel K2 = matrix(cellcounts), names hcenter
		putexcel K2 = "No MCS"
		
		putexcel N2 = "Total", hcenter
		putexcel K9 = "Total", hcenter
		
		putexcel N3 = formula(=SUM(L3:M3)), hcenter
		putexcel N4 = formula(=SUM(L4:M4)), hcenter
		putexcel N5 = formula(=SUM(L5:M5)), hcenter
		putexcel N6 = formula(=SUM(L6:M6)), hcenter
		putexcel N7 = formula(=SUM(L7:M7)), hcenter
		putexcel N8 = formula(=SUM(L8:M8)), hcenter
		putexcel L9 = formula(=SUM(L3:L8)), hcenter
		putexcel M9 = formula(=SUM(M3:M8)), hcenter
		putexcel N9 = formula(=SUM(L3:M8)), hcenter
		
	* IABP
		tab YEAR DIED if iabp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel P2 = matrix(cellcounts), names hcenter
		putexcel P2 = "IABP"
		
		putexcel S2 = "Total", hcenter
		putexcel P9 = "Total", hcenter
		
		putexcel S3 = formula(=SUM(Q3:R3)), hcenter
		putexcel S4 = formula(=SUM(Q4:R4)), hcenter
		putexcel S5 = formula(=SUM(Q5:R5)), hcenter
		putexcel S6 = formula(=SUM(Q6:R6)), hcenter
		putexcel S7 = formula(=SUM(Q7:R7)), hcenter
		putexcel S8 = formula(=SUM(Q8:R8)), hcenter
		putexcel Q9 = formula(=SUM(Q3:Q8)), hcenter
		putexcel R9 = formula(=SUM(R3:R8)), hcenter
		putexcel S9 = formula(=SUM(Q3:R8)), hcenter
		
	* pVAD
		tab YEAR DIED if tand_imp_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel U2 = matrix(cellcounts), names hcenter
		putexcel U2 = "pVAD"
		
		putexcel X2 = "Total", hcenter
		putexcel U9 = "Total", hcenter
		
		putexcel X3 = formula(=SUM(V3:W3)), hcenter
		putexcel X4 = formula(=SUM(V4:W4)), hcenter
		putexcel X5 = formula(=SUM(V5:W5)), hcenter
		putexcel X6 = formula(=SUM(V6:W6)), hcenter
		putexcel X7 = formula(=SUM(V7:W7)), hcenter
		putexcel X8 = formula(=SUM(V8:W8)), hcenter
		putexcel V9 = formula(=SUM(V3:V8)), hcenter
		putexcel W9 = formula(=SUM(W3:W8)), hcenter
		putexcel X9 = formula(=SUM(V3:W8)), hcenter
		
	* ECMO
		tab YEAR DIED if ecmo_tot == 1, row matcell(cellcounts)
		
		levelsof year_s, local(yearlabels)
		matrix rownames cellcounts = `yearlabels'
		
		levelsof died_s, local(diedlabels)
		matrix colnames cellcounts = `diedlabels'
		
		matrix list cellcounts
		
		putexcel Z2 = matrix(cellcounts), names hcenter
		putexcel Z2 = "ECMO"
		
		putexcel AC2 = "Total", hcenter
		putexcel Z9 = "Total", hcenter
		
		putexcel AC3 = formula(=SUM(AA3:AB3)), hcenter
		putexcel AC4 = formula(=SUM(AA4:AB4)), hcenter
		putexcel AC5 = formula(=SUM(AA5:AB5)), hcenter
		putexcel AC6 = formula(=SUM(AA6:AB6)), hcenter
		putexcel AC7 = formula(=SUM(AA7:AB7)), hcenter
		putexcel AC8 = formula(=SUM(AA8:AB8)), hcenter
		putexcel AA9 = formula(=SUM(AA3:AA8)), hcenter
		putexcel AB9 = formula(=SUM(AB3:AB8)), hcenter
		putexcel AC9 = formula(=SUM(AA3:AB8)), hcenter
		
	* Close log
		log close
		