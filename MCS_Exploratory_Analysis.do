capture log close	// close any open log files
version 15.1 		// make sure version is the same every time it's run
clear all 			// clear any open datasets
macro drop _all 	// drop all macros
set linesize 100 	// set line size for legibility
//ssc install groups	// install groups package if you haven't done so already

* Based on \\storage1.ris.wustl.edu\kjoyntmaddox\Active\Alina\Cardiovascular Race - Gmerice\Stroke Data and Code

* Set current directory
	cd "\\storage1.ris.wustl.edu\kjoyntmaddox\Active\Daniel\MCS Disparities Manoj"

* Create log file	
	log using "Output\MCS Exploratory Analysis", replace text

* Characterize all, surgery, and no surgery groups

use "New Data\cardshock17_full.dta"

gen I10_PR1_short = substr(I10_PR1,1,4)

gen I10_DX1_short = substr(I10_DX1,1,3)

groups I10_DX1_short, order(h) select(10)

groups I10_DX1_short if surgery == 1, order(h) select(10)

groups I10_DX1_short if surgery == 0, order(h) select(10)

* Analyze group 1 (ischemia)

use "New Data\cardshock17_ischemia.dta"

* * entire group
tab syshf_tot diahf_tot, cell

* * systolic heart failure pts
tab transplant_tot vad_tot if syshf_tot == 1, cell

* * diastolic heart failure pts
tab transplant_tot vad_tot if diahf_tot == 1, cell

* * neither systolic nor diastolic heart failure pts
tab transplant_tot vad_tot if syshf_tot == 0 & diahf_tot == 0, cell

* * top 10 diagnoses of group 1 pts
groups I10_DX1_short, order(h) select(10)

clear

////////////////////////////
* Analyze group 2 (no ischemia)

use "New Data\cardshock17_noischemia.dta"

* * entire group
tab syshf_tot diahf_tot, cell

* * systolic heart failure pts
tab transplant_tot vad_tot if syshf_tot == 1, cell

* * diastolic heart failure pts
tab transplant_tot vad_tot if diahf_tot == 1, cell

* * neither systolic nor diastolic heart failure pts
tab transplant_tot vad_tot if syshf_tot == 0 & diahf_tot == 0, cell

* * top 10 diagnoses of group 2 pts
groups I10_DX1_short, order(h) select(10)

* * top 10 diagnoses of systolic or systolic and diastolic CHF pts
tab FEMALE if syshf_tot == 1 | (syshf_tot == 1 & diahf_tot == 0)
groups I10_DX1_short if syshf_tot == 1 | (syshf_tot == 1 & diahf_tot == 0), order(h) select(10)

* * top 10 diagnoses of diastolic CHF pts
tab FEMALE if syshf_tot == 0 & diahf_tot == 1
groups I10_DX1_short if syshf_tot == 0 & diahf_tot == 1, order(h) select(10)

* * all diagnoses of group 2 pts with neither systolic nor diastolic heart failure, no VAD, and no transplant
tab I10_DX1_short if syshf_tot == 0 & diahf_tot == 0 & transplant_tot == 0 & vad_tot == 0

* * top 10 diagnoses of group 2 pts with neither systolic nor diastolic heart failure, no VAD, and no transplant
groups I10_DX1_short if syshf_tot == 0 & diahf_tot == 0 & transplant_tot == 0 & vad_tot == 0, order(h) select(10)

* * top 10 procedures of group 2 pts with neither systolic nor diastolic heart failure, no VAD, and no transplant
groups I10_PR1_short if syshf_tot == 0 & diahf_tot == 0 & transplant_tot == 0 & vad_tot == 0, order(h) select(10)

clear

log close